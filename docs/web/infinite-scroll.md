## `thymeleaf` `axios` `ajax` - infinite scroll
> [참고자료](https://write.corbpie.com/infinite-scrolling-with-ajax-calls/)

### 코드
```html
<!doctype html>
<html lang="ko" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Infinite Scroll with axios</title>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.27.2/axios.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.1.slim.min.js" integrity="sha256-w8CvhFs7iHNVUtnSP0YKEg00p9Ih13rlL9zGqvLdePA=" crossorigin="anonymous"></script>
    <script th:inline="javascript">
        /*<![CDATA[*/
        var count = 0;
        var currentscrollHeight = 0;

        // 브라우저 시작 시 10개의 데이터 가져옴
        jQuery(document).ready(function ($) {
            console.log("start api")
            for(let i = 0; i < 10; i++){
                bring_data(count)
                count++;
            }
        })

        // scroll 할 때마다 function() 실행
        try{
            $(window).on("scroll", function (){
                console.log('infinite_scroll() ...')

                const scrollHeight = $(document).height();
                const scrollPos = Math.floor($(window).height() + $(window).scrollTop());
                const isBottom = scrollHeight - 100 < scrollPos;

                if(isBottom && currentscrollHeight < scrollHeight){
                    for(var i = 0; i < 6; i++){
                        bring_data(count);
                        count++;
                    }
                    currentscrollHeight = scrollHeight;
                }
            });
        }catch (e){
            console.log("데이터 없지롱~~")
        }


        async function bring_data(count){
            console.log('bring_data() ...')
            console.log(`${axios}`);

            await axios.get('https://jsonplaceholder.typicode.com/todos')
                .then(function (res){
                    // 기본 응답 타입: Object
                    console.log(`json array >> ${res.data}`);
                    console.log(`data size: ${res.data.length}`);

                    let itemPerPage = 10; // 10개씩 보여주게끔
                    // let totalPages = Math.ceil(res.data.length / itemPerPage);

                    // quis ut nam facilis et officia qui
                    console.log(`data array[1]: ${res.data[1].title}`);


                    console.log(`data[${count}]: ${res.data[count].title}`);
                    $('<div>' + res.data[count].title + '</div>' + count).appendTo('.item');

                })
        }
        /*]]>*/
    </script>
</head>
<body>
    <div class="container">
        <!-- item div -->
        <div class="item">

        </div>
    </div>
</body>
</html>
```

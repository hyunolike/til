## css(fade-in) 와 <meta> 태그 이용해 splash 구현할래
- ![image-1.png](./image-1.png)

### 소스코드 - splash.html
```java
<!doctype html>
<html lang="ko" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="refresh" content="3; url=http://localhost:8080/view?userId=1">
<!--    <meta http-equiv="page-enter" content="blendTrans(duration=3)">-->
<!--    <meta http-equiv="page-exit" content="blendTrans(duration=3)">-->
    <title>bring-data</title>
</head>
<style>
    body {
        background-color: black;
    }
    h1 {
        color: white;
    }
    img {
        position: absolute;
        width: 100%;
        height: 100%;
        object-fit: cover;
        z-index: -1;
        transform: scale(1.8);
        animation: scaleImage 5s ease-out forwards;
    }
    .fade-in-box {
        display: inline-block;
        background: darkcyan;
        padding: 10px;
        animation: fadein 3s;
        -moz-animation: fadein 3s; /* Firefox */
        -webkit-animation: fadein 3s; /* Safari and Chrome */
        -o-animation: fadein 3s; /* Opera */
    }
    @keyframes fadein {
        from {
            opacity:0;
        }
        to {
            opacity:1;
        }
    }
    @-moz-keyframes fadein { /* Firefox */
        from {
            opacity:0;
        }
        to {
            opacity:1;
        }
    }
    @-webkit-keyframes fadein { /* Safari and Chrome */
        from {
            opacity:0;
        }
        to {
            opacity:1;
        }
    }
    @-o-keyframes fadein { /* Opera */
        from {
            opacity:0;
        }
        to {
            opacity: 1;
        }
    }
</style>
<body>
    <h1 class="fade-in-box">splash</h1>
</body>
</html>
```

## Thymeleaf with ajax
### 구성
![image.png](./image.png)

### 개발 순서
1. Controller
2. View(Thymeleaf)

#### 1. Controller
```java
@Controller
public class StartController {

    // http://localhost:8080/view?userId=12
    @GetMapping("/view")
    public String startView(Model model, @RequestParam String userId){
        model.addAttribute("data", userId);

        return "start" ;
    }
}
```

#### 2. View
```java
<!doctype html>
<html lang="ko" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Start View + Axios</title>
    <!--  axios cdn  -->

<!--    <script type="text/javascript" th:href="@{/js/index.js}"></script>-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.27.2/axios.min.js"></script>
    <script th:inline="javascript">

        // [CDATA 를 사용해서 자바스크립트에서 모델에서 지정한 값 호출 실시 : 주석까지 필수 등록]
        /*<![CDATA[*/
        function bring_data() {
            var bringData = /*[[${data}]]*/;

            console.log(`CDN 적용 확인 로그: ${axios}`);
            console.log("use axios libraray ...");

            axios.get('https://jsonplaceholder.typicode.com/todos/' + bringData)
                .then(function (result){
                    console.log(`통신결과 ${JSON.stringify(result.data)}`);
                    const h1El = document.querySelector('h1');
                    h1El.textContent = JSON.stringify(result.data.title);
                });
        }
        bring_data();
        /*]]>*/
    </script>
</head>
<body>
<!--    <h3>Thymeleaf + Axios</h3>-->
    <div th:text="${data}">default data</div>
    <!--  데이터 출력 부분~~~  -->
    <h1></h1>
</body>
</html>
<!--thymeleaf + javascript + model 참고자료: https://kkh0977.tistory.com/1176-->
```

## jquery, mouseup
### index.html
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link rel="stylesheet" href="index.css" />
  </head>
  <body>
    <div id="test" style="padding-top: 5rem">
      테스트테스트테스트테스트테스트테스트테스트테스트테스트테스트테스트테스트테스트테스트테스트
      테스트테스트테스트테스트테스트테스트테스테스트테스트테스트테스트테스트테스트테스테스트테스트테스트테스트테스트테스트테스
      테스트테스트테스트테스트테스트테스트테스테스트테스트테스트테스트테스트테스트테스
    </div>
    <script
      src="https://code.jquery.com/jquery-3.6.1.slim.min.js"
      integrity="sha256-w8CvhFs7iHNVUtnSP0YKEg00p9Ih13rlL9zGqvLdePA="
      crossorigin="anonymous"
    ></script>
    <script type="text/javascript" src="index.js" />
  </body>
</html>
```

### index.js
```javascript
var pageX, pageY;

$('#test').mousemove(function (event) {
  var msg = '하이 내 위치야';
  msg += event.pageX + ', ' + event.pageY;

  pageX = event.pageX;
  pageY = event.pageY;

  console.log(msg);
});

$('#test').mouseup(function () {
  var odv = document.createElement('div');
  var odvText = document.createTextNode('새로 추가한 요소 :)');
  odv.appendChild(odvText);

  odv.className = 'item';

  odv.style.left = pageX + 10 + 'px';
  odv.style.top = pageY - 30 + 'px';

  console.log(`left: ${pageX} ${odv.style.left}`);
  console.log(`top: ${pageY}`);

  document.body.append(odv);
});

```
### index.css
```css
.item {
  position: fixed;
  background-color: aqua;
}

```

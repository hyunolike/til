# 더 자바, 애플리케이션을 테스트하는 다양한 방법
## 1. `Junit 5`
> [참고 자료](https://intrepidgeeks.com/tutorial/junit5-organize-junit5-configuration-declaration-and-support)
- 자바 개발자가 많이 사용하는 테스트 프레임워크
- 2017년 10월
### `@BeforeAll` `@AfterAll`
- 모든 테스트 실행 전 딱 1번만 실행!
- `static` 메서드 사용

### `@BeforeEach` `@AfterEach`
- 모든 테스트 실행 전 딱 1번만 실행!
- 그냥 void만 사용

![image-1.png](./image-1.png)

### `@Disabled`
- 해당 테스트 함수 실행 ❌

### 테스트 이름 표기하기
- `@DisplayNameGeneration`
- `@DisplayName` >> 이모지도 사용 가능 ✔ 권장!!
![image-2.png](./image-2.png)

## 2. JUnit 5 | `Assertion` ⭐
> `import static org.junit.jupiter.api.Assertions.*;`
- `assertEquals(StudyStatus.happy, study.getStatus),() -> "메시지");
    - `() ->`: 실패할 때만 문자열 연산 ✔ 성능 신경쓰는 입장에서는 유리
    - 그냥 메시지는 실패 성공 구분없이 모두 연산
- `assertAll()`
    - 검사 1개가 앞에서 깨져도 다른 모든 것도 검증! ✔
```java
assertAll(
        () -> assertNotNull(study),
        () -> assertEquals(StudyStatus.DRAFT, study.getStatus(), () -> "스터디를 처음 만들면 DRAFT"),
        () -> assertTrue(1 < 2)
);
```
![image-3.png](./image-3.png)
- 이외에도 서드파티 다양한 것들이 있다. `assertThat` 

### 조건에 따라 테스트 실행
- `assumeTrue()`
- ...

### 태깅과 필터링
- 테스트 그룹을 만들고 원하는 테스트 그룹만 테스트를 실행할 수 있는 기능

### 커스텀 태그

### 테스트 반복
- `@RepeatedTest()`
- ![image-5.png](./image-5.png)
- `@ParameterizedTest` > JUnit5에서 제공!
- ![image-6.png](./image-6.png)
```java
@ParameterizedTest
@ValueSource(strings = {"날씨가", "많이", "추워지고", "있네요"})
void parameterizedTest(String message){
    System.out.println(message);
}
```
- `@ValueSource()`
- `@CsvSource()` : 콤마로 구분

### 테스트 인스턴스
- 테스트 순서는 예측 불가 > JUnit 5 작성 순서대로 진행되지만 그래도 예측 불가하다 ✔ (정해져 있는 것이 아님)
    - 그렇기 때문에 테스트 의존도 ❌ 만들어야됨!!
- `@TestInstance()`

### 테스트 순서 `시나리오 테스트` 
- 내부적으로 정해진 순서가 있지만 순서에 의존하면 안됨 ✔
    - 다른 테스트 코드에 영향 주면 안되니까
- 경우에 따라서는 원하는 순서로 하고 싶다
- `@TestMethodOrder()`
    - `@Order()` 스프링꺼말고 junit 제공하는 걸로!

### JUnit 5 설정 파일 `junit-platform.properties`
- 경로 classpath 설정하기(선작업)
- ![image-7.png](./image-7.png)
- ![image-8.png](./image-8.png)

### 확장 모델
- JUnit 5에서 단순해짐! 확장모델
- `Extension`

### JUnit 5 마이그레이션

## 3. `Mockito`
- `Mock`: 진짜 객체와 비슷하게 동작하지만 프로그래머가 직접 그 객체의 행동을 관리하는 객체
- 실제 프로젝트 진행 시
    - 단위 테스트의 단위를 어떻게 정의
    - mock은 어느 범위에서까지 mock을 정할 것인가
        - 강사님은 이미 구현된 클래스를 모킹할 필요 없다고 생각
        - 외부서비스는 목킹 필요가 있다
- `Argument matchers` 
### `Mockito` 테스트 상황 및 생성 방법
- interface만 존재하고 구현체는 존재하지 않는다. >> 이럴경우 Mock을 사용하기 적절한 경우
#### `mock()` 
```java
MemberService memberService = mock(MemberService.class);
StudyService studyService = mock(StudyService.class);
```
#### `@Mock` 
- 크게 2가지
    - 선언
    - 매개변수로 사용

```java
@ExtendWith(MockitoExtension.class) ⭐ 설정해줘야 된다!
...
@Mock 
MemberService memberService;
@Mock
StudyService studyService;
```
### `Mockito` 객체 Stubbing
- 모든 mokito 객체의 행동
    - Null 리턴(Optional 타입은 Optional.empty 리턴)
    - Primitive 타입 기본 Primitive 값.
    - 콜렉션은 비어있는 콜렉션
    - Void 메소드는 예외를 던지지 않고 아무런 일도 발생하지 않는다.

- Mock 객체를 조작해서
    - 특정한 매개변수를 받은 경우 특정한 값을 리턴하거나 예외를 던지도록 만들 수 있다
        - `when(memberService.findById(1L)).thenReturn(Optional.of(member));`
        - `when(memberService.findById(1L)).thenThrow(new RuntimeException());`
    - Void 메소드 특정 배개변수를 받거나 호출된 경우 예외를 발생 시킬 수 있다
        - `doThrow(new IllegalArgumentException()).when(memberService).validate(1L);`
    - 메소드가 동일한 매개변수로 여러번 호출될 때 각기 다르게 행동하도록 조작

```java
@Test
void createStudyServic(@Mock MemberService memberService, @Mock StudyRepository studyRepository){
    StudyService studyService = new StudyService(memberService, studyRepository);
    assertNotNull(studyService);

    Member member = new Member();
    member.setId(1L);
    member.setEmail("hyunho@gmail.com");

    // stubing
    when(memberService.findById(1L)).thenReturn(Optional.of(member)); ⭐ 여기서 넘기는 파라미터 중요!
    when(memberService.findById(any())).thenReturn(Optional.of(member)); ⭐ argument >> 위와 같은 문제를 해결! 어떠한 값도 들어가짐

    Study study = new Study(10, "java");

    Optional<Member> findById = memberService.findById(1L);
    assertEquals("hyunho@gmail.com", findById.get().getEmail());
}
```

### `Mock` 객체 확인 
- 해당 목 로직? 몇번 호출됬는지 확인 가능! ✔
```java
verify(memberService, times(1)).notify(study); // 해당 로직 1번 호출
verify(memberService, never()).validate(any()); //전혀 호출 x
```
- 로직 순서 테스트 ✔
```java
InOrder inOrder = inOrder(memberService);
inOrder.verify(memberService).notify(로직1);
inOrder.verify(memberService).notify(로직2);
inOrder.verify(memberService).notify(로직3);
```

### `Mockito` BDD 스타일 API ⭐
- `BDD`: 애플리케이션이 어떻게 '행동' 해야 하는지에 대한 공통된 이해를 구성하는 방법 >> __TDD__ 에서 창안
- 행동에 대한 스팩
    - Title
    - Narrative
        - As a(어떤 역할) / I want(~하고 싶다) / so that(내가 갈 수 있다는 의사를 표현하도록~)
    - Acceptance criteria
        - Given(주어진 상황) / When(~하면) / Then(이럴것이다)

```java
@DisplayName("다른 사용자가 볼 수 있도록 스터디를 공개한다.")
@Test
void test2(@Mock MemberService memberService, @Mock StudyRepository studyRepository){
    //Given
    StudyService studyService = new StudyService(memberService, studyRepository);
    Study study = new Study(10, "더 자바, 테스트");
    assertNull(study.getOpenedDateTime());
    given(studyRepository.save(study)).willReturn(study);

    //When
    studyService.openStudy(study);

    //Then
    assertEquals(study.getStatus(), StudyStatus.OPENED);
    assertNotNull(study.getOpenedDateTime());
    then(memberService).should().notify(study);
}
```

## 4. 도커와 테스트 `TestContainers` ✔
- 테스트에서 도커 컨테이너 사용
    - 스크립트 작성 및 테스트 설정, 수작업으로 시작/종료 안해도 됨!
- ⭐ 개발, 운영, 테스트 모두 같은 db를 쓰고 싶다! 인메모리 h2 db가 아닌!!
    - db 자체가 다르면 여러가지 차이 분명히 발생!!!
    - 트랜잭션 `isolation` 레벨,  `propagation` >> DB마다 기본 전략이 다르다
    - 스프링의 기본 전략은 DB를 따름
- 테스트 진행 시 마다 도커 띄어주고, 다 썼으면 비워주고 반복 ㅠ,ㅠ >> 간단하게 해결 
- 스크립트 테스트 설정 띄우고 내리고 할 필요가 사라짐

## 5. 아키텍처 테스트 `ArchUnit`
- 유즈 케이스
    - 해당 패키지에서만 사용하는지
    - 해당 클래스들의 특정 의존도 확인 가능
    - 해당 클래스의 위치 확인 가능
    - 특정 스타일 아키텍처 검증


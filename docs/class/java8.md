## 자바8
- `2014년 3월` 출시
- 개발자 __83% 정도__ 사용
- LTS vs 비 LTS [버전별 차이](https://www.oracle.com/java/technologies/java-se-support-roadmap.html)
    - LTS: 배포 주기 3년 / 실제 운영 환경에서는 LTS 버전 권장 / ✔ 다음 LTS는 __자바 21__ 
    - 비 LTS: 업데이트 제공 기간 짧다 / 배포 주기 6개월
    - 매년 3월/9월 새 버전 배포


![image-9.png](./image-9.png)
- 주요 기능
    - 람다 표현식
    - 메소드 레퍼런스
    - 스트림 API
    - Optional<T>
    - ...
- JDK 
    - 오라클 JDK
    - 오픈 JDK(오라클, AdoptOpenJDK, Amazon Corretto, Azul Zulu ...)

### 1. 함수형 인터페이스와 람다
- 인터페이스 >> ⭐오직 추상메서드 1개면 함수형 인터페이스
- 정의할려면 `@FuntionalInterface`
- 자바에서 함수형 프로그래밍 ✔
    - 함수를 `First class object` 로 사용
    - 순수 함수(Pure function) ⭐
        - 사이드 이팩트 만들 수 없다.(함수 밖에 있는 값을 변경 x)
        - 상태 없다.(함수 밖에 정의되어 있는)
    - 고차 함수(High-Order Function)
        - 함수가 함수를 매개변수로 받을 수 있고 함수를 리턴할 수 있다.
    - 불변성

```java
@FunctionalInterface ✔
public interface RunSomething {
    void doIt();
    static void printName(){
        System.out.println("hyunho");
    }
    default void printAge(){
        System.out.println("40");
    }
}
```

- 기존(자바8이전)

```java
RunSomething runSomething = new RunSomething() {
    @Override
    public void doIt() {
        System.out.println("hello");
    }
};
```

- 개선 (자바8이후 / 람다) ✔

```java
RunSomething runSomething = () -> System.out.println("hello");
```

#### 😁함수형 인터페이스 사용 예시
- 따로 클래스 만들어서 생성

```java
public class Plus10 implements Function<Integer, Integer> {
    @Override
    public Integer apply(Integer integer) {
        return integer + 10;
    }
}
```

- 바로 생성(메인 클래스) ✔

```java
Funtion<Integer, Integer> plus10 = (i) -> i + 10;
System.out.println(plus10.apply(1));
```

- 함수 2개 이상 사용하기 ✔ `compose` `andThen`

```java
Function<Integer, Integer> plusFun = (i) -> i + 10;
Function<Integer, Integer> multiply2 = (i) -> i * 2;

plus10.compose(multiply2); // 입력값을 가지고 먼저 뒤에 오는 함수 적용 > 그 결과값을 입력 값으로 사용
>> 이렇게도 사용 가능 Function<Integer, Integer> multiply2AndplusFun = plusFun.compose(multiply2); 

plusFun.andThen(multiply2).apply(2) // 함수 순으로 실행
```

#### 😁람다
- 쉐도잉이 일어나지 않음

#### 😁메소드 레퍼런스
- 람다가 하는 일이 기존 메소드 또는 생성자를 호출하는 거라면, 메소드 레퍼런스를 사용해서 __매우 간결하게 표현 가능__ ✔

```java
✔ UnaryOperator: 입출력 타입이 동일한 경우 사용! 
UnaryOperator<String> hiFun = (i) -> "hi " + i;
UnaryOperator<String> hi = Greeting::hi; ⭐ 간단하게 표현

Supplier<Greeting> newGreeting = Greeting::new; ⭐ 
Greeting greeting = newGreeting.get();// 인스턴스 생성

String[] names = {"일","hyunho", "Jang"};

Arrays.sort(names, String::compareToIgnoreCase);
System.out.println(Arrays.toString(names));// [hyunho, Jang, 일]
```

### 2. 인터페이스 변화
- 인터페이스의 __기본메서드__ 와 __스태틱 메서드__ ✔

```java
public interface Foo {
    void printName();
    
    default void printNameUpperCase(){ ✔ 기본 메서드 >> 하위 클래스 모두 이 기능을 가지게 됨(따로 추가 x)
        System.out.println("Foo");
    };
}
```

#### 😁자바8에 추가된 기본 메소드로 인한 API 변화
- `Iterable`의 기본 메소드
    - forEach()
    - spliterator()

```java
Spliterator<String> spliterator = name.spliterator();
while(spliterator.tryAdvance(System.out::println));
```

- `Collection`의 기본 메소드
    - stream() / parallelStream()
    - removeIf(Predicate)
    - spliterator()
- `Comparator`의 기본 메소드 및 스태틱 메소드
    - reserved()
    - thenComparing()
    - static reverseOrder() / naturalOrder()
    - static nullsFirst() / nullsLast()
    - static comparing()

### 3. 스트림 API
- 공장에서 제품 만드는 것으로 비유 ✔
- 스트림 __파이프라인__ (0또는 다수의 중개 오퍼레이션 + 한개의 종료 오퍼레이션)
    - 중개 오퍼레이션: `Stream` 리턴 ⭕
    - 종료 오퍼레이션: `Stream` 리턴 ❌


```java
long k = name.stream().map(String::toUpperCase)
        .filter(s -> s.startsWith("K"))
        .count();
```

### 4. `Optional`
- `null` 체크

### 5. `Date` `Time`
```java
Instant instant = Instant.now();
System.out.println(instant); // 기준시 UTC, GMT

ZonedDateTime zonedDateTime = instant.atZone(ZoneId.systemDefault());
System.out.println(zonedDateTime);

LocalDateTime now = LocalDateTime.now();
System.out.println(now);

LocalDateTime.of(1995, Month.DECEMBER, 13, 0, 0, 0); //월은 0부터 시작!
ZonedDateTime nowInKorea = ZonedDateTime.now(ZoneId.of("Asia/Seoul"));
System.out.println(nowInKorea);

2022-04-29T05:24:10.705232Z
2022-04-29T14:24:10.705232+09:00[Asia/Seoul]
2022-04-29T14:24:10.730641700
2022-04-29T14:24:10.730973500+09:00[Asia/Seoul]
```

### ✔ 요약
- 자바8의 주요 기능의 의미
    - 함수형 프로그래밍
        - 함수형 인터페이스
        - 람다 표현식
        - 메소드 레퍼런스 
    - 비동기 프로그래밍
        - CompletableFuture
    - 편의성 개선
        - Optional
        - Date & Time
        - 인터페이스
        - ...
    

## 개발 관련 툴 자료 
- [`Intellij` 마우스 휠로 글씨 크기 조절](https://github.com/hyunolike/info-docs/blob/main/tool/intellij-mouse-size.md)
- [`WinMerge` 텍스트 및 파일 비교 무료 프로그램](https://github.com/hyunolike/info-docs/blob/main/tool/winmerge.md)
- [`postman` 스크립트 사용](https://github.com/hyunolike/info-docs/blob/main/tool/postman-script.md)
- [`PlantUML` intellij에서 사용하는 방법](https://github.com/hyunolike/info-docs/blob/main/tool/plantuml-intellij.md)
- [`intellij` git confilct 확인](https://github.com/hyunolike/info-docs/blob/main/tool/intellij-git-confilct.md)
- [`postman` 변수](https://github.com/hyunolike/info-docs/blob/main/tool/postman-variable.md)
- [`sourcetree` 깃랩 게정 연결](https://github.com/hyunolike/info-docs/blob/main/tool/sourcetree-add-gitlab.md)
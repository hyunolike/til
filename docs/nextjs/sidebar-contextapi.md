## sidebar 개발 - `context api`
### 문제
- 사이드 바 구조
    - `useState` 훅은 해당 반복되는 부모&자식 노드별 각각 존재하고 있음
- ![Alt text](image.png)
```
부모 노드
ㄴ 자식 노드

* 메뉴 갯수만큼 반복
```
### 해결
- 전역 상태값 사용! `context api`
- ![Alt text](image-1.png)
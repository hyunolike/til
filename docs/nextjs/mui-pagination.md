## MUI framework 이용한 커스텀 페이지네이션 개발 + Spring Boot paging api
### 준비사항
- MUI
    - Pagination
- React
    - useState()

#### 전체구조
```js
const Pagination = ({inquiry}) => {

    ...
    // 페이지네이션 값
    const [page, setPage] = useState(0);

    const handlePage = async() => {
        setPage(1); 

        const res = Service.getPage();
        ...
    }

    return (
        <>
            <Pagination 
                value={page}
                onChange={() => {
                    handlePage(...)
                }}
            />

            <MultiItem
                // 해당 프레임워크 value 값 가져오는 방법
                onChange={(event, value) => {
                    ...(value)
                }}
            >
            </MultiItem>
        </>
    )

}
```
## 문서작성
- [`UML` 클래스 다이어그램](https://github.com/hyunolike/info-docs/blob/main/docs/class-diagram.md)
- [PlantUML - 코드로 다이어그램 작성하기](https://plantuml.com/ko/sitemap)
- [⭐개발 일정 산정](./dev-plan.md)
- [⭐도메인 주도 설계 - `클래스 다이어그램`](https://github.com/hyunolike/info-docs/blob/main/docs/ddd-class-diagram.md)
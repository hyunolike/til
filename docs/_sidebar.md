- [Overview](README.md)
- 💡`Oracle`
    - [`level` for문처럼~~](./api/oracle/level.md)
- 💡`Java API`
    - [`Character.getNumericValue()`](./api/java/char-value.md)
    - [`Map` - `entrySet()` `Map.Entry()`](./api/java/map-info.md)
    - [`Stream` 최종연산 max(), min() 같은거 정수로 받기?](./api/java/stream-final.md)
    - `addAll()` 합집합, `removeAll()` 차집합, `retainAll` 교집합
    - [자바 정렬 방법 ~ `List.sort()`](./api/java/list-sort.md)
    - [`PriorityQueue`](./api/java/priorityqueue.md)
    - [`Collection` 2차원](./api/java/Collection-2.md)
    - [`Arrays`](./api/java/Arrays.md)
    - [`Stream`](./api/java/Stream.md)
    - [`ArrayList` - removeIf(), forEach()](./api/java/ArrayList-coll.md)
    - [`Collections` - frequency(Collection c, 값)](./api/java/Collectons-frequency.md)
    - [`Deque`](./api/java/Deque.md)
- 💡`JavaScript API`
    - [`reduce`](./api/javascript/reduce.md)
    - [표준 내장 객체](./api/javascript/standard-objects.md)
- 💡`Architecture`
    - [⚡NCP - 아키텍처 참조 사이트 바로가기](https://www.ncloud.com/intro/architecture)
    - [📌React + Spring Boot](./architecture/react-spring.md)
    - [📌Database + master(source) <-> slave(replica) 개념](./architecture/db.md)

- 문서
    - [📚![](https://img.shields.io/badge/Seminar-192446?logo=Seminar&style=flat-square) Docs](./seminar/index.md)
    - [📚![](https://img.shields.io/badge/Training-192446?logo=Training&style=flat-square) Docs](./training/index.md)
    - [📚![](https://img.shields.io/badge/Spring-192446?logo=Spring&style=flat-square) Docs](./dev/spring/index.md)
    - [📚![](https://img.shields.io/badge/Java-192446?logo=Java&style=flat-square) Docs](./dev/java/index.md)
    - [📚![](https://img.shields.io/badge/Kotlin-192446?logo=Kotlin&style=flat-square) Docs](./dev/kotiln/index.md)
    - [📚![](https://img.shields.io/badge/JavaScript-192446?logo=JavaScript&style=flat-square) Docs](./dev/javascript/index.md)
    - [📚![](https://img.shields.io/badge/TypeScript-192446?logo=TypeScript&style=flat-square) Docs](./dev/typescript/index.md)
    - [📚![](https://img.shields.io/badge/Cloud-192446?logo=AmazonAWS&style=flat-square) Docs](./dev/cloud/index.md)
    - [📚![](https://img.shields.io/badge/DB-192446?logo=DB&style=flat-square) Docs](./dev/db/index.md)
    - [📚![](https://img.shields.io/badge/Git-192446?logo=Git&style=flat-square) Docs](./dev/git/index.md)
    - [📚![](https://img.shields.io/badge/Server-192446?logo=Seminar&style=flat-square) Docs](./server/index.md)
    - [📚![](https://img.shields.io/badge/Web-192446?logo=Seminar&style=flat-square) Docs](./web/index.md)
    - [📚![](https://img.shields.io/badge/React-192446?logo=React&style=flat-square) Docs](./react/index.md)
    - [📚![](https://img.shields.io/badge/Security-192446?logo=Security&style=flat-square) Docs](./security/index.md)
    - [📚![](https://img.shields.io/badge/API_Platform-192446?logo=Platform&style=flat-square) Docs](./platform/index.md)
    - [📚![](https://img.shields.io/badge/NCP-192446?logo=Naver&style=flat-square) Docs](./ncp/index.md)
    - [📚![](https://img.shields.io/badge/Android-192446?logo=Android&style=flat-square) Docs](./android/index.md)
    - [📚![](https://img.shields.io/badge/문서작성-192446?logo=문서작성&style=flat-square) Docs](./doc/index.md)
     - [📚 ![](https://img.shields.io/badge/Next.js-000?logo=Next.js&style=flat-square) Docs](./nextjs/index.md)
    - [📚![](https://img.shields.io/badge/Vue.js-000?logo=Vue.js&style=flat-square) Docs](./vue/index.md) 
    - [📚 ![](https://img.shields.io/badge/CSS3-000?logo=css3&style=flat-square) Docs](./css/index.md)
    - [📚 ![](https://img.shields.io/badge/html5-000?logo=html5&style=flat-square) Docs](./html/index.md)
     - [📚 ![](https://img.shields.io/badge/.NET-512BD4?logo=.NET&style=flat-square) Docs](./dotnet/index.md)
    - [📚개발 툴 관련 Docs](./tools/index.md)
    - [📚테스트 관련 Docs](./test/index.md)
    - [📚배포 관련 Docs](./deploy/index.md) 
    - [📚DevOps Docs](./devops/index.md) 
    - [📚 `🤖chatGPT` Docs](./chatgpt/index.md)
    - [📚`jira` `confluence` Docs](./jira/index.md)
    - [📚QA Docs](./qa/index.md) 
    - [📚Infra etc Docs](./infra/index.md)
    - [📚Python Docs](./python/index.md)
    - [⚡NAVER Open Source 사이트 바로가기](https://naver.github.io/)
- 개발
    - [![](https://img.shields.io/badge/Next.js-000?logo=Next.js&style=flat-square) `MUI` + useState() 이용한 페이지네이션 + Spring Boot paging api](./nextjs/mui-pagination.md)
    - [![](https://img.shields.io/badge/Next.js-000?logo=Next.js&style=flat-square) 사이드바 개발 `context api`](./nextjs/sidebar-contextapi.md)
    - [⭐ `nexus` 구축 방법 - docker 기반 :)](./training/nexus.md)
    - [⭐️ `react` - axios / proxy / react-router](./training/react-project.md)
    - [⭐ mouseup + jquery ](./web/drag-div.md)
    - [⭐ infinite scroll - `axios` `thymeleaf` `ajax`](./web/infinite-scroll.md)
    - [⭐ `<meta>` `css(fade-in)` - splash page 구현하기이](./web/splash-page.md) 
    - [⭐ `Thymeleaf` + `ajax`](./web/thymeleaf-ajax.md)
    - [⭐ 웹단 모바일 환경에 맞춰 `@media` 설정](./web/mediaquery.md)
    - [⭐ JPA 외래키 설정해보자](./dev/spring/jpa-foreignkey.md)
    - [⭐ `Oauth2.0` 사용 시 세션이 생성되는 이유](./server/oauth-session.md)
    - [⭐ `react-modal` 실습~](./react/react-modal.md)
    - [⭐ `Jsoup` 자바로 파서할래 크롤링ㅋㅋㅋ](./dev/java/jsoup.md)
    - [⭐ `Prometheus` 실습](./server/prometheus-gogo.md)
    - [⭐ `Oauth 2.0`](./server/oauth.md)
    - [⭐ `Prometheus` 사용 팁](./server/prometheus.md)
    - [⭐️ `Redis DB` 왜 사용해?? 회원 & Spring Security & JWT ](dev/spring/redis-login.md)
    - [⭐️ `Spring Security` `Oauth` 사용해 로그인하기](#) - [참고링크](https://velog.io/@swchoi0329/%EC%8A%A4%ED%94%84%EB%A7%81-%EC%8B%9C%ED%81%90%EB%A6%AC%ED%8B%B0%EC%99%80-OAuth-2.0%EC%9C%BC%EB%A1%9C-%EB%A1%9C%EA%B7%B8%EC%9D%B8-%EA%B8%B0%EB%8A%A5-%EA%B5%AC%ED%98%84)
    - [⭐️ `Mocha`를 왜 스프링부트 서비스에 적용하는 거야? - **api 테스트**](dev/spring/mocha.md)
    - [⭐`React` 프로젝트 폴더 구조 잡아보자](react/react-folder.md)
    - [⭐Spring Data JPA - 외래키로 데이터 조회 방법](./dev/spring/foreign-key-select.md)
    - [⭐React 상태관리 이론](react/react-statue-management.md)
    - [github submodule](https://choieungi.github.io/posts/git-submodule/)
    - [⭐`Mocha` + `Spring Boot` 테스트 해보자](./training/mocha-springboot.md)
    - [⭐`JWT` 로그인&로그아웃](./training/jwt-user.md)
    - [⭐React + Spring Boot + JWT](./training/react-spring-jwt.md)
    - [⭐React 배포](./training/react-deploy.md)
    - [⭐️`Spring Boot` + `React` 배포방식 요약](./training/spring-react.md)
    - [⭐`Sendbird` 연동 스프링 부트 프로젝트](./training/sendbird-spring.md)
    - [⭐batch api return value](./dev/spring/batch-api.md)
    - [⭐`Jenkins` 실습](./dev/spring/jenkins.md)
    - [⭐Gradle 멀티 프로젝트 관리하기](./dev/spring/multi-gradle.md)
    - [⭐RabbitMQ 실습](./dev/spring/rabbitmq-gogo.md)
    - [⭐RestTemplate 서버단 연결 예외처리하기](./dev/spring/servertoserver.md)
    - [⭐JSON API를 개발해보자](./dev/api/index.md)
    - [⭐JPA ManyToOne 관계 테스트해보자](./dev/spring/manytoone.md)
    - [⭐javadoc](./dev/spring/javadoc.md)
    - [🍃`Gradle` - Task 설정](./dev/spring/gradle-task.md)
    - [🍃`Groovy`](./dev/spring/groovy.md)
    - [🍃JPA Specification](./dev/spring/specification.md)
    - [🍃스프링 배너 변경하기](./dev/spring/banner.md)
    - [🍃ConcurrentHashMap](./dev/spring/hashmap.md)
    - [🍃ModelMapper](./dev/spring/modelmapper.md)
    - [🍃JSch](#)
    - [🍃메시지 큐](./dev/spring/messageque.md)
    - [🍃rabbitmq](./dev/spring/rabbitmq.md)
    - [🍃ObjectMapper](#)
    - [🍃Spring Security 유저 정보 받아오기](./dev/spring/user-info.md)
    - [🍃Spring Security](./dev/spring/security.md)
    - [🍃SpringSecurity 커스텀 유저 데이터베이스 만드는 법](./dev/spring/userdetails.md)
    - [🍃DTO Inner Class 사용하자](./dev/spring/dto-inner-class.md)
    - [🍃매개변수 넘겨받는 방법](./dev/spring/parameter.md)
    - [🍃Response 응답 종류 및 방법](./dev/spring/response.md)
    - [🍃AOP](./dev/spring/aop.md)
    - [🍃Gradle 멀티모듈](./dev/spring/gradle-multi-module.md)
    - [🍃JPA - @Query](./dev/spring/query.md)
    - [🍃@Transactional](./dev/spring/transcational.md)
    - [🍃스프링 빈](./dev/spring/bean.md)
    - [🍃@Component @Bean](./dev/spring/cb.md)
    - [☕Inner Class & Anonymous Class](./dev/java/java-class.md)
    - [☕Google Java Style Guide](./dev/java/guide.md)
    - [☕Stream API ![](https://img.shields.io/badge/%EC%9D%B8%EA%B8%B0-42B983?style=flat-square)](./dev/java/stream.md)
    - [☕argument vs parameter](./dev/argument.md)
    - [☕Integer vs int](./dev/int.md)
    - [☕JSON(JavaScript Object Notaion)](./dev/json.md)
    - [☕추상 클래스 vs 인터페이스](./dev/abs.md)
    - [☕자바 향상된 for문](./dev/for.md)
    - [☕자바 람다식 메서드 참조 -> ::](./dev/lambda.md)
    - [🐳도커](./dev/cloud/docker.md)
    - [🐳도커 요약](./dev/cloud/docker-tip.md)
    - [🐳스프링부트 도커 이미지 빌드 및 실행](./dev/cloud/docker-img.md)
    - [🐳spring boot+mysql+docker compose 실습](./dev/cloud/docker-compose.md)
    - [:octocat: git stash](./dev/etc/git-stash.md)
    - [🙄Over-Fetching & Under-Fetching](./dev/etc/fetching.md)
    - [🙄Gradle 의존성 옵션](./dev/etc/gradle-option.md)
    - [🙄인텔리제이 자동으로 import 기능](./dev/etc/intellij.md)
    - [🙄javac vs java](./dev/etc/javajava.md)
    - [🙄Sftp](#)
    - [🙄세션(+ 네트워크 연결)](./dev/etc/session.md)
    - [🙄쉘](./dev/etc/shell.md)
    - [🙄배치](./dev/etc/batch.md)

-  OJT
    - [⭐코드 리뷰](./ojt/code-review.md)
    - [⭐OJT 기간동안 발생한 이슈 ![](https://img.shields.io/badge/%EC%9D%B8%EA%B8%B0-42B983?style=flat-square)](./ojt/ps.md)
    - [⭐테스트코드 어떻게 짜야될까?](./ojt/tdd.md)
    - [⭐서버 구조](./ojt/server.md)
    - [🙄기존 ssh키 등록하기](./ojt/ssh.md)
    - [🙄인텔리제이-디버깅하기](./ojt/debugging.md)
    - [🙄메시지 큐](#)
    - [🐳도커 배포하기](./ojt/docker-gogo.md)
    - [🐳도커 강의](./ojt/docker.md)
    - [🐳도커 컴포즈](./ojt/docker-compose.md)
    - [🐳도커 볼륨](./ojt/docker-volume.md)
    - [🐳도커 네트워크](./ojt/docker-network.md)
    - [📍Redis](./ojt/redis.md)
    - [🍃JPA 연관 관계 매핑 종류 + 영속성 전이](./ojt/jpa.md)
    - [🍃시큐리티 로그인 이해](./ojt/login.md)
    - [🍃UserDetailsService의 loadUserByUsername() 함수 로직](./ojt/userdetails.md)
    - [🍃DTO 설계-중첩 클래스](./ojt/dto.md)
    - [🍃JWT 기본 설계 구조](./ojt/jwt.md)
    - [🍃JWT? ![](https://img.shields.io/badge/%EC%9D%B8%EA%B8%B0-42B983?style=flat-square)](./ojt/jwt-study.md)
    - [🍃MockMvc](#) 
- ![](https://img.shields.io/badge/Naver_Cloude_Platform-fff?logo=Naver&style=flat-square)
    - [Compute]()

- Tools
    - [DB Diagram](#)
    - [소스코드 시각화 툴](#)
    - [시스템 구성도 그리기 툴](tools/diagram.md)

- 참고 자료
    - [도메인 패키지 구조](https://cheese10yun.github.io/spring-guide-directory/)
    - [주 작업 표시줄 옮기기](https://m.blog.naver.com/rsritter/221860618060)
    - [Elasticsearch](https://tecoble.techcourse.co.kr/post/2021-10-19-elasticsearch/)
    - [![](https://img.shields.io/badge/Google-000?logo=Google)구글링 방법](dev/etc/google_search.md)
    - [화이트 해커 교육 솔루션 사이트](https://raonctf.com/essential/main/web)
    - [깃허브 프로젝트 참고 자료](https://github.com/boostcampwm-2021/iOS05-Escaper/wiki/PR-Template)

- 수업
    - [인프런-`더 자바, 애플리케이션을 테스트하는 다양한 방법`](./class/the-java-test.md)
    - [인프런- `JAVA 8`](./class/java8.md)


## `Sendbird` `Spring Boot`
- Skill


```gradle
dependencies {
    description("common library")
    implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
    implementation 'org.springframework.boot:spring-boot-starter-validation'
    implementation 'org.springframework.boot:spring-boot-starter-web'
    compileOnly 'org.projectlombok:lombok'
    runtimeOnly 'org.mariadb.jdbc:mariadb-java-client'
    annotationProcessor 'org.projectlombok:lombok'
    testImplementation 'org.springframework.boot:spring-boot-starter-test'
}

dependencies {
    description("WebClient")
    implementation group: 'org.springframework.boot', name: 'spring-boot-starter-webflux', version: '2.7.2'
}

dependencies {
    description("Swagger")
    implementation group: 'io.springfox', name: 'springfox-swagger2', version: '2.9.2'
    implementation group: 'io.springfox', name: 'springfox-swagger-ui', version: '2.9.2'
}

dependencies {
    description("Flyway")
    implementation group: 'org.flywaydb', name: 'flyway-core', version: '7.7.3'
}
```
### 1. 패키지 구조
- ![image-1.png](./image-1.png)

### 2. ERD
- ![image-2.png](./image-2.png)

### 3. 구조
- ![image-4.png](./image-4.png)

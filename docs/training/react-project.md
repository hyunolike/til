## React 
- axios: fetch 도 있지만 json 다루기 더 편함
- react-router: 페이지 라우터
- http-proxy-middleware: 스프링부트 쪽과의 통신

### 1. 실습 순서
1. 각 라이브러리 다운로드
2. react-router 설정
3. http-proxy-middleware 설정
4. axios 설정

### 2. react-router
```javascript
<BrowserRouter>
<div style={{padding: 20, border: '5px solid gray'}}>
    <Link to="/">메인 화면</Link><br/>
    <Link to="/mona">모나 화면</Link><br/>
    <Routes>
    <Route exact path="/" element={<Home />}/>
    <Route path="/mona" element={<Mona />}/>
    </Routes>
</div>
</BrowserRouter>
```

### 3. http-proxy-middleware
```javascript
const {createProxyMiddleware} = require('http-proxy-middleware');

module.exports = (app) => {
    app.use(
        createProxyMiddleware('/api', {
            target: 'http://localhost:12345',
            changeOrigin: true
        })
    )
}
```

### 4. axios
```javascript
import axios from "axios";
import {useState} from "react";

const Home = () => {

    // axios({
    //     url: '/users?user_id=MONA-USER33'
    // })

    const [users, setUsers] = useState(null);
    const [errors, setErrors] = useState(null);

    const apiSubmit = async () => {
        try {
            setUsers(null);
            const response = await axios.get(
                '/api/users?user_id=MONA-USER99'
            );
            console.log(response.data)
            setUsers(response.data)
        }catch (e){
            setErrors(e);
        }
    }

    if(errors) return <div>에러 발생 ㅠ,ㅠ</div>

    return (
        <div>
            <h1>모나 챗 홈</h1>
            <br/>
            <button onClick={apiSubmit}>유저 조회 api</button>
            <br/>
            <br/>
            <ul>
                {JSON.stringify(users)}
            </ul>
        </div>

    )
}
```

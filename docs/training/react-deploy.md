## 리엑트 배포 
- 기존 리엑트를 로컬에서 개발 서버 구동 방식 >> `npm run start` >> 작은 웹서버 가동!! ✔ 이를 자동으로 인식!!
    - 소스코드에 정적으로 분석 가능한 오류 있다면 개발서버를 통해 어느 부분에서 에러 발생했는지 안내
- 실제 배포시는 웹서버 선택하여 해당 웹서버에 리엑트를 배포 ⭐

### 1. 핵심 흐름
> [참고자료](https://blog.naver.com/PostView.naver?blogId=ywsu22&logNo=222432582712&parentCategoryNo=&categoryNo=9&viewDate=&isShowPopularPosts=true&from=search)
1. 프로젝트 빌드 / 빌드파일 생성
2. 웹서버 설치 / 설정(접속할 포트 / 랜더링할 파일 루트 설정 / 랜더링할 파일 설정 등 필요에 따라 다름)
3. 빌드한 파일을 미리 웹서버에 설정한 랜더링할 파일 루트로 이동
4. 웹서버 재구동
5. 결과 확인!!




## nexus 구축
> [참고자료](https://www.bearpooh.com/48)
- ![image-9.png](./image-9.png)
### Docker 사설 저장소 3개
|저장소 명|설명|
|----|----|
|`hosted(Local)`|내부에서 생성한 Docker 이미지 파일 배포(Push)|
|`proxy(Remote)`|외부의 Docker 저장소의 이미지들을 저장하고 내부 사용자에게 전달 / 캐시 역할|
|`group(Repos)`|다수의 hosted, proxy 저장소 묶어 단일 경로 제공|

### 구축 순서
#### 1. blob store 생성
- Docker 통해 공유되는 패키지 파일들을 저장할 공간 설정

#### 2. Docker Repository 생성
##### `hosted(local)`
- 신규 생성한 Docker 이미지 배포 시, 내부에서만 사용하고 외부 배포 제한할 때 유용
- docker pull 기본적으로 `5000` 포트 사용 하지만, `docker(group)` 사용하기 위해 `5001` 지정!
##### `proxy(remote)` 
- docker(proxy) 선택해 proxy 저장소 생성
- 외부 저장소 연결 시 사용!
##### `group(repos)`
- `hosted` `proxy` 저장소 묶어 하나의 저장소 설정할 때 사용!

#### 3. Docker Realms 설정
- pull 하면 **인증** 필요하다는 메시지 출력 ㅠ,ㅠ
- ✔ `Nexus3` 인증 관련 설정 담당 >> `Realms` 설정에서 Docker 대한 권한 추가

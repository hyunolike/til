## `Mocha` 테스트 라이브러리 + `Spring Boot` 
> [참고자료](https://javanitto.tistory.com/34)

### 1. 개발 순서
1. mocha 폴더 따로 생성 후, `npm init` 초기 상태
2. build.gradle 파일 이용해 라이브러리 의존성 추가 `mocha`
3. `mocha` 테스트 코드 연동 ...(진행중) 22.08.25

#### 1.1 mocha 폴더 따로 생성 후, `npm init` 초기 상태
- ![image-6.png](./image-6.png)

#### 1.2 build.gradle 파일 이용해 라이브러리 의존성 추가 `mocha`
```groovy
plugins {
    id 'org.springframework.boot' version '2.7.3'
    id 'io.spring.dependency-management' version '1.0.13.RELEASE'
    id 'java'
    // Node.js 스크립트 실행위한 플러그인
    id 'com.github.node-gradle.node' version "3.4.0" ⭐
}

group = 'com.example'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '1.8'

repositories {
    mavenCentral()
}

dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-web'
    testImplementation 'org.springframework.boot:spring-boot-starter-test'
}

tasks.named('test') {
    useJUnitPlatform()
}

// node task
// https://javanitto.tistory.com/34
node {
    download = false
    npmInstallCommand = "install"
    nodeProjectDir = file("${project.projectDir}/mocha")
}

tasks.register("npm-install"){
    group = "applicaton"
    tasks.appNpmInstall.exec()
}

task appNpmInstall(type: NpmTask){
    workingDir = file("./mocha")
    args = ["install", "mocha"]
}
```
- ![image-7.png](./image-7.png)
- ![image-8.png](./image-8.png)

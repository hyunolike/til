## 퍼블리싱
- [통 이미지에 있는 버튼 동작하게 만들기 `<a>` 태그 활용](https://github.com/hyunolike/info-docs/blob/main/publishing/a.md)
- [통 이미지에 있는 영역을 다른 이미지로 덮어씌우기 - 유튜브 iframe](https://github.com/hyunolike/info-docs/blob/main/publishing/iframe.md)
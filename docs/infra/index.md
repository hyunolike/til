## Infra
- [⭐️개발자도 알아야돼! `CPU` 아키텍처](https://github.com/hyunolike/info-docs/blob/main/infra/cpu.md)
- [IP 확인 및 고정 설정](https://github.com/hyunolike/info-docs/blob/main/infra/ip.md)
- [HTTPS 원리 이해](https://github.com/hyunolike/info-docs/blob/main/infra/https.md)
- [⭐️ssl 인증서](https://github.com/hyunolike/info-docs/blob/main/infra/secure-sockets-layer.md)
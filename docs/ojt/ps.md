# ⭐ OJT 기간동안 발생했던 `Issue` 모음 :)
## 1. 왜 로그인이 안될까...? 
### 문제
- 이미 개발된 로그인 API를 이용해 추가적인 API 개발을 하는 상황! 하지만 그 과정에서 로그인 후 `JWT` 토큰을 이용한 유저값을 받아야되는데 😫 회원가입은 되는데 로그인 자체가 안되는 문제
### 해결
- 알고보니 API 명세서 잘못 기재된 부분을 수많은 삽질은 통해 깨달음..ㅎ(자세한 부분은 아래 그림 참고)


![image-10.png](./image-10.png)

## 2. 로그인 정보와 함께 계산서버 요청 보내기
### 문제
- 로그인 정보가 유지되지 않는 환경 속, 계산 서버에 해당 로그인 정보를 담아 보내는 방법을 모르겠다.

### 해결
- `JWT` 토큰을 헤더에 담아서 요청해주자!


![image-11.png](./image-11.png)

## 3. APP SERVER 통해서 계산 서버 정상 응답 받기
### 문제
- 요청은 성공이지만 값이 `NULL` 로 받아오는 이슈

### 해결
- 계산 서버의 리턴값과 `RestTemplate` 로 받는 DTO가 다르다는 사실을 알게됨


![image-12.png](./image-12.png)

## 4. json 객체 배열말고 그냥 배열로 응답받기
### 문제
- json 객체 배열이 아닌 그냥 배열로 하는 과정 중 로직 구성의 어려움

### 해결
```java
    public UserListDTO userInfo() {
        List<UserMapping> result = userRepository.findAllBy(); ✔ 1. 특정 값만 추출하게 커스텀
        List<String> info = result.stream() ✔ 2. 특정 키의 값 배열로 변환
                .map(UserMapping::getPassword)
                .collect(Collectors.toList());

        UserListDTO userDTO = UserListDTO.builder() ✔ 3. 다시 DTO에 담아 설정
                .password(info)
                .build();
        return userDTO;
    }
```
![image-29.png](./image-29.png)

## 5. 도커 컴포즈 포트 연결 이슈
### 문제 
- `application.properties` 에서 설정한 `server.port=9090` 과 `docker-compose.yml`에 설정한 포트가 서로 연결이 안되는 문제점
- ![image-31.png](./image-31.png) 

### 해결
![image-33.png](./image-33.png)


## 6. `RestTemplate` 다른 서버와의 통신 연결 이슈
### 문제
- 기존 `http://localhost:포트번호/...` POST 방식 연결 자체 안되는 문제 

### 해결
- __도커 컴포즈__ 에 설정한 서버 이름을 해당 주소에 매핑 `http://server_name:포트번호/...`


![image-34.png](./image-34.png)


## `docker compose`
> [도커 컴포즈 개념서 같은 자료 바로가기](https://www.baeldung.com/ops/docker-compose)
> [docker-compose로 nginx + spring-boot + mysql 구성하기](https://joont92.github.io/docker/docker-compose%EB%A1%9C-nginx-spring-boot-mysql-%EA%B5%AC%EC%84%B1%ED%95%98%EA%B8%B0/)
- '구성하다' >> 여러 컨테이너를 모아서 하나의 어플리케이션 구성하는 것 ⭐
- `YAML` 파일 사용 >> 애플리케이션 서비스 구성
- 컨테이너 동작 시킬 때 > 도커의 기본 네트워크 대역 + 별도의 네트워크 대역폭 지정 > 배포


![image-21.png](./image-21.png)
- `eth0(검정)`: 컨테이너 내에서 동작하는 os의 ip주소 갖고있는 NIC(Network Interface Card)
- `veth(virtual ethernet)`: eth0에게 외부 네트워크 정보를 전달해주어 네트워크 기능 사용할 수 있도록 해주는 역할
- `docker 0 (브릿지)`: 요청된 패킷의 경로를 알맞은 컨테이너로 전달해주는 역할 / 특정 네트워크 대역을 가진 브릿지 역할 수행

### 1. `docker-compose.yml`  설정 용어 파악하기
```yml
version: "3.7" 🐳 compose파일 형식 버전
services: 🐳 하나 이상의 서비스
    ...
volumes: 🐳 선택적 볼륨
    ...
networks: 🐳 선택적 네트워크
    ...
```
- `docker-compose up`

#### 1-1. 서비스
- 🐳 3개의 다른 서비스를 구성요소 3개의 이미지 분할 후, 서로 다른 서비스로 정의!!
```yml
services:
    frontend:
        image: my-vue-app
        ...
    backend:
        image: my-spring-app
        ...
    db:
        image: postred
```

#### 1-2. 볼륨 및 네트워크
- `volume`: 호스트와 컨테이너 또는 컨테이너 간에 공유되는 디스크 공간의 물리적 영역. 모든 컨테이너에서 볼 수 있는 __호스트의 공유 디렉토리__
- `network`: 컨테이너 간, 컨테이너와 호스트 간의 통신 규칙 정의


### 2. 서비스 분석
#### 2-1. 이미지 가져오기
```yml
services:
    my-service:
        image: ububtu:latest 🐳 이미지 이름과 태그를 지정해 이미지 속성 참조
        ...
```

#### 2-2. 이미지 구축
- `Docerfile` 읽어 소스 코드에서 이미지 빌드!
```yml
services:
    my-custom-app:
        build: /path/to/dockerfile/
        ...
```        

```yml
services:
    my-custom-app:
        build: https://github.com/my-company/my-project.git 🐳 경로도 사용 가능
        ...
```

```yml
services:
    my-custom-app:   🐳 빌드 속성과 이미지 이름 지정 / 생성된 이미지의 이름을 지정 다른 서비스에서 사용할 수 있도록 해준다.
        build: https://github.com/my-company/my-project.git 
        image: my-project0-name 
        ...
```

#### 2-3. 네트워크 구성
- Docker 컨테이너 > 암시적, Docker Compose에 의해 생성된 네트워크에서 서로 통신

```yml
services:
    network-example-service:
        image: karthequian/helloworld:latest
        expose:
            - "80"
```
- 호스트 > 컨테이너 포트=포트키워를 통해 선언적으로 노출
```yml
services: 🐳 포트80은 이제 호스트에 볼수 있고
    network-example-service:
        image: karthequian/helloworld:latest
        ports:
            - "80:80"
        ...
    my-custom-app: 🐳 포트3000은 호스트의 포트8080, 8081에서 사용 가능!
        image: myapp:latest
        ports:
            - "8080:3000"
        ...
    my-custom-app-replica:
        image: myapp:latest
        ports:
            - "8081:3000"
        ...
```
- 위의 강력한 메커니즘을 통해 충돌없이 동일한 포트를 노출하는 다른 컨테이너 실행 가능해짐

- ⭐ 컨테이너 분리위해 추가 가상 네트워크 정의 가능!
```yml
services:
    network-example-service:
        image: karthequian/helloworld:latest
        networks:
            - my-shared-network
        ...
    another-service-in-the-same-network:
        image: alpine:latest
        networks:
            - my-shared-network
        ...
    another-service-in-its-own-network:
        image: alpine:latest
        network:
            - my-private-network
        ...
networks:
    my-shared-network: {}
    my-private-network: {}
```

#### 2-4. 볼륨 설정
- 크게 3가지? > 요즘에는 명명된 볼륨이 권장!
```yml
services:
    volumes-example-service:
        image: alpine:latest
        volumes:
            - my-named-global-volume:/my-volumes/named-global-vlume
            - /tmp:/my-volumes/host-volume
            - /home:/my-volumes/readonly-host-volume:ro 🐳 ro == 읽기 전용
            ...
    another-volumes-example-service:
        image: alpine:latest
        volumes:
            - my-named-global-volume:/another-path/the-same-named-global-volume
            ...
volumes:
    my-named-global-volume:
```

#### 2-5. 종속성 선언
- `depends_on` 키워드
```yml
services:
    kafka:
        image: wurstmeister/kafka:2.11-0.11.0.3
        depends-on:
            - zookeeper
        ...
    zookeeper:
        image: wurstmeister/zookeeper
        ...
```

### 3. 환경 변수 관리
- `${ }` 표기법
```yml
services:
    database:
        image: "postgres:${POSTGRES_VERSION}"
        environment:
            DB: mydb
            USER: "${USER}"
```


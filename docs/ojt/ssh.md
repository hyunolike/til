## `ssh` 키 등록하기
1. 기존에 만들었던 ssh키 private키를 따로 저장해두기
2. 해당 private키 source tree에 등록

![image-30.png](./image-30.png)

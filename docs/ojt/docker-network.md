## 🐳Docker network
- 도커 컨테이너끼리 통신할때 사용
- 도커 기본 네트워크 `bridge` `hosts` `none` 3가지 네트워크 만듦

---
## 네트워크, 컨테이너간 통신
> [참고 자료](https://xmobile.tistory.com/entry/Docker-%EB%84%A4%ED%8A%B8%EC%9B%8C%ED%81%AC-%EC%BB%A8%ED%85%8C%EC%9D%B4%EB%84%88%EA%B0%84-%ED%86%B5%EC%8B%A0)
- ![image-36.png](./image-36.png)
- 3개의 네트워크
    - `bridge`: 기본 네트워크 드라이브 / 드라이브 지정하지 않는 경우 default bridge 사용
    - `host`: 호스트 네트워킹 직접 사용
    - `none`: 모든 네트워킹 비활성화 
### 실습
- ![image-37.png](./image-37.png)
- 같은 네트워크에 있는 컨테이너간만 통신 가능
- 사용자가 만든 외부 네트워크를 이용하면 컨테이너 이름으로 연결 가능

---
## Docker Network 구조 `docker0`
![image-38.png](./image-38.png)



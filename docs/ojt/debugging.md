## 디버깅(Debugging)
- 버그(오작동) 현상을 해결 및 이러한 오류들을 찾기 위한 __테스트 과정__
    - 문제를 찾는 과정
### Break Point
- 중단점, 중지점
- 프로그램을 의도적으로 잠시 또는 완전히 멈추게 하는 장소


### Intellij
> [참고자료](https://jojoldu.tistory.com/149)
- `resome`: 다음 break point 이동
- `step over`: 현재 break 된 파일에서 다음 라인 이동
- `step into`: 현재 break 된 라인에서 실해앟고 있는 라인으로 이동



![image-23.png](./image-23.png)


![image-22.png](./image-22.png)
- ✔ 브레이크 포인트에 조건을 걸어줄수도 있다.


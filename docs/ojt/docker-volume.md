## Docker volume > 영속적으로 저장
> [참고자료](https://www.daleseo.com/docker-volumes-bind-mounts/)


![image-16.png](./image-16.png)
![image-17.png](./image-17.png)
![image-20.png](./image-20.png)


- 컨테이너 삭제하면 데이터도 같이 삭제되는 문제 >> 볼륨으로 해결하자!
- 도커 컨테이너의 경우 쉽게 내리고 올릴 수 있다. 특별하게 설정해주지 않으면 함께 내려가 삭제됨

---
## Docker volume 상세히
> [참고 자료](https://0902.tistory.com/6)
- 도커 >> 컨테이너 안의 파일 변경 사항을 `UnionFS` 를 통해 관리
    - `UnionFS`는 이미지 layer + write layer 합쳐 컨테이너 데이터 관리, 컨테이너 삭제 시 write layer도 삭제
    - ✌여기서 write layer는 이미지 layer의 데이터에서 변경된 사항을 저장하므로 write layer 삭제 시 데이터  사라짐( __데이터 휘발성__ )

### 언제 사용? 데이터를 컨테이너끼리 공유 또는 호스트 저장
- 컨테이너의 데이터 휘발성 때문에 데이터를 컨테이너가 아닌 __호스트__ 에 저장할 때
- 또는 컨테이너끼리 데이터 공유할 때 볼륨 사용

### 도커 볼륨 사용법
1. `docker run -it -v (컨테이너의 volume 디렉토리) (이미지) /bin/bash`
    - ✔ 이 방법은 컨테이너의 데이터를 호스트에 유지할 때 사용 가능
    - 하지만, 컨테이너 삭제되면 데이터 찾기 힘들어짐 ㅠ,ㅠ 추천하지 않음
2-1. `docker run -it -v (호스트 디렉토리):(컨테이너의 volume 디렉토리) (이미지) bin/bash`
    - ✔ 호스트의 특정 디렉토리(or 파일)을 컨테이너와 매핑
    - 볼륨 위치를 사용자가 정할 수 있으니까 데이터 찾기 쉬움
2-2. `docker run -it --name container1 -v /root/data:data centos /bin/bash` +  `docker run -it --name container2 -v /root/data:data centos /bin/bash` 
    - ✔ 여러개의 컨테이너에서 파일을 공유하고 할 때 사용 가능
3. `docker run -it -v (호스트 파일):(컨테이너의 파일) centos /bin/bash`
    - ✔ 디렉토리 뿐아니라 호스트의 파일 하나도 container와 매핑가능





## Prometheus 실습
> [참고자료](https://velog.io/@zihs0822/MySQL-Exporter-Prometheus-%EC%97%B0%EB%8F%99)
### 1. 먼저, 도커 컴포즈로 프로메테우스, 그라파나 다 띄우기
```yml
version: '3.1'

services:
  grafana:
    image: grafana/grafana-oss:9.0.2
    container_name: MMIS_monitor_grafana
    ports:
      - 3441:3000
    volumes:
      - /var/lib/grafana
      - ./grafana/grafana.ini:/etc/grafana/grafana.ini
    restart: unless-stopped
    logging:
      options:
        max-size: "10m"
    links:
      - prometheus

  prometheus:
    image: prom/prometheus
    container_name: MMIS_monitor_prometheus
    volumes:
      - ./prometheus.yml:/etc/prometheus/prometheus.yml
    ports:
      - 3440:9090
    command:
      - '--web.enable-lifecycle'              # http://localhost:3440/-/reload
      - '--storage.tsdb.retention.time=30d'   # metrics 보존 기간
      - '--config.file=/etc/prometheus/prometheus.yml'
    restart: unless-stopped
    logging:
      options:
        max-size: "10m"

  node_exporter:
    image: prom/node-exporter:latest
    container_name: MMIS_monitor_node-exporter
    ports:
      - 9100:9100
    command: -collectors.enabled=diskstats,filesystem,loadavg,meminfo,netdev,stat,time,uname

  mysqld_exporter:
    image: prom/mysqld-exporter:latest
    container_name: MMIS_monitor_mysqld-exporter
    ports:
    - 9104:9104
    environment:
      - "DATA_SOURCE_NAME=exporter:exporter@localhost"
    command:
      - --collect.info_schema.processlist
```

### 2. `Prometheus.yml` 파일 작성
```yml
# default 값 설정하기 - 여기 부분은 전부 설정 안해줘도 상관없음
global:
  scrape_interval: 15s # scrap target의 기본 interval을 15초로 변경 / default = 1m
  scrape_timeout: 15s # scrap request 가 timeout 나는 길이 / default = 10s
  evaluation_interval: 2m # rule 을 얼마나 빈번하게 검증하는지 / default = 1m
  external_labels:
    monitor: 'MMIS_secret_chat_server-monitor'

scrape_configs:
  - job_name: 'MMIS_secret_chat_server-monitoring' # 잡 이름
    metrics_path: '/actuator/prometheus'
    # Override the global default and scrape targets from this job every 5 seconds.
    static_configs:
      - targets: ['host.docker.internal:12345'] # 도커 인스턴스 내부 호스트

  - job_name: 'MMIS_secret_chat_RDB-monitoring'
    params:
      collect[]: ['collect.info_schema.processlist']
    static_configs:
      - targets: ['host.docker.internal:9104']

```
- ![image.png](./image.png)

### 3. 프로메테우스 동작 확인
![image-1.png](./image-1.png)

### 4. 그라파나 >> 프로메테우스 연결
![image-2.png](./image-2.png)

### 5. 끝
![image-3.png](./image-3.png)

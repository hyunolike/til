## Oauth 2.0 로그인 성공 후 세션 생성되는 이유  
> [참고자료](https://ugo04.tistory.com/166?category=868740) <br>
> [참고자료](https://sjh836.tistory.com/165) <br>
> [참고자료](https://minsoolog.tistory.com/14)
- 스프링 시큐리티 >> filter기반 동작 / spring mvc 와 분리되어 관리 및 동작
- 기본적으로 전통적인 쿠키-세션 방식 사용!
- 스프링 지원 로그인 방식 
    - 폼로그인 `UsernamePasswordAuthentication`
    - ajax 로그인 `Basic Authentication` 요청 헤더에 토큰 담아 로그인 하는 방식
    - OAuth2 로그인 (로그인 api 위임하는 방식)
    - JWT 토큰 방식 (SPA, 모바일 환경에서 JWT(Json Web Token) 생성하여 인증하는 방식)

### 세션 생성되는 로직
> 결국에는 `AccessToken` 얻기 위해서는 다시한번 OAuth 서버에 요청을 날려야 됨 그럼 이걸 토대로 서버에서 받아 저장하고 클라이언트 서버(리엑트)에도 전달해주면 됨!
- ![image-4.png](./image-4.png)

### 보안 용어
- ![image-5.png](./image-5.png)


|용어|사용 함수|
|----|--------|
|접근 주체 `Principle`|`Authentication` 생성 <br> SecurityContext 보관되어 사용(security 세션들은 내부 메모리(`SecurityContextHolder`)에 쌓고 꺼내쓰는 것)|
|인증 `Authenticate`|HttpSecurity 인증 API|
|인가 `Authorize`|HttpSecurity 인가 API|
|권한|`FilterSecurityInterceptor` ???|

# Platform
- [`sendbird`](https://github.com/hyunolike/info-docs/blob/main/platform/sendbird.md)
- [`sendbird` - Platform API `User`](https://github.com/hyunolike/info-docs/blob/main/platform/sendbird-user.md)
- [`sendbird` - Platform API `Channel`](https://github.com/hyunolike/info-docs/blob/main/platform/sendbird-channel.md)
- [`sendbird` - Platform API `Message`](https://github.com/hyunolike/info-docs/blob/main/platform/sendbird-message.md)

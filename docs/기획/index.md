# 🦄 기획
- [좋은 에러 메시지 > 6가지 원칙](https://github.com/hyunolike/planning-development-docs/blob/develop/web/error-message.md)
- [로딩](https://github.com/hyunolike/planning-development-docs/blob/develop/web/loading.md)
- [딥 링크](https://github.com/hyunolike/planning-development-docs/blob/develop/app/deeplink.md)
# 🐤Seminar
- [`Armeria` - ![](https://img.shields.io/badge/LINE-FFF?logo=LINE&style=flat-square)](https://github.com/hyunolike/info-docs/blob/main/seminar/armeria.md) >> [📌참고 링크](https://www.youtube.com/watch?app=desktop&d=n&feature=youtu.be&fbclid=IwAR2VSoSk3YeHxHmEDsP-gkLg6QI-_q0ht9MgRKklAxqk_DOiAxmJth0IaBc&v=jlKb61lHOOw)
- [![](https://img.shields.io/badge/%EC%9A%B0%EC%95%84%ED%95%9C%ED%85%8C%ED%81%AC%EC%84%B8%EB%AF%B8%EB%82%98-5DBEBA)![](https://img.shields.io/badge/kafka-231f20?logo=Apache-Kafka)실시간 음식배달 플랫폼에서 활용한 분산 이벤트 스트리밍](#)
- [`ATDD` 인수테스트주도개발](https://www.youtube.com/watch?v=ITVpmjM4mUE&t=4s)
- [`Kakao` - 이모티콘 서비스 왜 MSA 선택](https://github.com/hyunolike/info-docs/blob/main/seminar/kakao-msa.md)
- [`타다` 시스템 아키텍처](https://github.com/hyunolike/info-docs/blob/main/seminar/tada.md) - [참고 자료](https://engineering.vcnc.co.kr/2019/01/tada-system-architecture/)
- [만들어 가며 알아보는 React: React는 왜 성공했나](https://techblog.woowahan.com/8311/)

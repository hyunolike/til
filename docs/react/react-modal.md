## `react-modal`
> [참고자료](https://codesandbox.io/s/9xnen?file=/src/App.js:276-302)
### ✔ 주의사항
- CRA 할때는 >> `npx create-react-app [프로젝트 명] --template typescript`
- `react-modal` 설치시 >> `@types/react-modal` `react-modal` 동시에 설치해야됨!!!
### `App.tsx`
```typescript
import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Modal from 'react-modal';

const App = () => {
  const [isOpen, setIsOpen] = useState(false);

  function toggleModal() {
    setIsOpen(!isOpen);
  }

  return (
    <div className="App">
      <button onClick={toggleModal}>Open modal</button>

      <Modal
        isOpen={isOpen}
        onRequestClose={toggleModal}
        contentLabel="My dialog"
        className="mymodal"
        overlayClassName="myoverlay"
        closeTimeoutMS={500}
      >
        <div>My modal dialog.</div>
        <button onClick={toggleModal}>Close modal</button>
      </Modal>
    </div>
  );
};

export default App;

```
### `App.css`
```typescript
.App {
  font-family: sans-serif;
  text-align: center;
}

.mymodal {
  position: fixed;
  top: 20%;
  left: 50%;
  transform: translate(-50%, -50%);

  border: 1px solid #ccc;
  background: #fff;
  overflow: auto;
  border-radius: 4px;
  outline: none;
  padding: 20px;
}

.myoverlay {
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.75);
}

.ReactModal__Overlay {
  opacity: 0;
  transition: opacity 500ms ease-in-out;
}

.ReactModal__Overlay--after-open {
  opacity: 1;
}

.ReactModal__Overlay--before-close {
  opacity: 0;
}
```

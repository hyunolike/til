## React Folder Architecture
> [참고자료](https://thinkforthink.tistory.com/373) <br>
> [✔`Atomic design` 리엑트 폴더 구조 관련 참고자료](https://fourwingsy.medium.com/react-%ED%94%84%EB%A1%9C%EC%A0%9D%ED%8A%B8%EC%9D%98-%EB%94%94%EB%A0%89%ED%86%A0%EB%A6%AC-%EA%B5%AC%EC%A1%B0-bb183c0a426e)


|폴더명|설명|
|------|-----------|
|`pages/`|route에 따라 랜딩되는 컴포넌트들을 둠 / `url`에 해당되는 페이지 컴포넌트|
|`components/`|페이지 이외의 컴포넌트로 모든 페이지에서 공통으로 사용하는 common 폴더와 각 페이지의 이름의 폴더들로 나눔|
|`constants/`|`컴포넌트 외` primary color와 같은 모든 페이지에서 주요하게 쓰이는 색 등의 상수 위치|
|`styles/`|globals.css, normalize.css, font.css 등 전역 css 스타일과 관련된 파일 위치|
|`hooks/`|공통으로 사용하는 커스텀 훅스 위치|
|`contexts/`|전역에 쓰이는 react context 모듈 위치|
|`libs/`|공통으로 사용하는 함수 위치|
|`apis/`|...|
|`....`|...|


### 비교
|프로젝트 1|프로젝트 2|
|-----------|----------|
|![image-2.png](./image-2.png)|![image-1.png](./image-1.png)|

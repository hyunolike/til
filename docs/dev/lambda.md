## `::` 의미
- 람다식 메서드 참조
    - 람다식이 하나의 메서드만 호출할 경우, 메서드 참조를 통해 람다식을 간단하게 표시 가능
- `클래스 명 :: 메서드 명` 또는 `참조변수 :: 메서드 명`

```java
//기존
Function<String, Integer> f = (String s) -> Integer.parseInt(s);

//메서드 참조
Function<String, Integer> f = Integer::parseInt

---
Supplier<MyClass> s = () -> new MyClass(); // 람다식
Supplier<MyClass> s = MyClass::new; //메서드 참조

---
Function<Integer, int[]> f = x -> new int[x]; //람다식
Function<Integer, int[]> f2 = int[] :: new; //메서드 참조
```

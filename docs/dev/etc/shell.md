## `shell` `bash` `zbash` 개념 정리
- [자료1](https://ithub.tistory.com/205)
- [`Bash` 핵심 요약 정리](https://velog.io/@nathan29849/Linux-binbash%EC%99%80-binsh%EC%9D%98-%EC%B0%A8%EC%9D%B4%EC%A0%90)
---
### `Linux` 계열 운영체제에서 사용하는 거 > `shell script`
- 텍스트 형식으로 저장되는 프로그램으로서 한줄씩 순차적으로 읽어 실행되도록 작성된 프로그램
- `shell`을 사용해 컴퓨터에 시킬 명령을 텍스트로 작성하여 실행시키는 것 뿐
- `script` > 인터프리터 방식으로 동작하는 컴파일 되지 않는 프로그램


![image-6.png](./image-6.png)

### 쉘이란
- 커널과 사용자 사이 이어주는 역할
- 하나의 명령어 처리기
- cmd도 하나의 쉘 


### 쉘의 종류
|종류|설명|
|:---:|-----|
|`sh`|- 쉘의 한 종류<br>- 초기 유닉스 쉘로 태어났다는 의미로 `Bourne shell` 줄임말로 `sh`로 불림<br>- 프롬프트: `$`|
|`bash`<br>현 시대에 가장 많이 쓰이는 쉘|- 쉘의 한 종류<br>- `Bourne-agin shell` 줄임말로 `bash`<br>- 1987년 브라이언 폭스에 의해 만들어짐<br>- `sh`와 대부분 호환<br>- 프롬프트: `#`|
|`bashrc`|- `bash` 사용할 때, 참고할 사항을 정의해 높은 파일|
|`zbash`|- 쉘의 한 종류<br>- `bash` `ksh` `tcsh` 등 일부 기능을 포함하고 개선한 확장형 쉘<br>- bash보다 command promt가 이쁨<br>- 사용하기 편리(자동완성 기능)<br>- 프롬프트: `%`|


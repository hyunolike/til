## :octocat: `git stash`
- 브랜치 체크아웃시 주의사항!!
    - 현재 작업디렉토리가 깨끗해야 됨 >> ✔ 갑작스럽게 체크아웃이 필요하다면?
- 해결 방법?? 작업 중인 내용의 임시 저장 `커밋을 이용한 방법`
    1. 브랜치1에서 일단(임시) 커밋하기 >> __불필요 커밋...ㅎ__
    2. 브랜치2로 체크아웃하고 볼 일 보기
    3. 다시 브랜치1로 되돌아 온다?
    4. 1의 작업을 이어서 하낟
    5. 커밋 덮어쓰기 (`commit --amend`) 하기
    6. (옵션) 필요하다면 (`push --force`) 하기
- 쓸모없는 커밋 발생...ㅎ 덮어쓰기 하자 `commit --amend`
    - `소스트리` > 오른쪽 상단(마지막 커밋 정정) ✔

![image.png](./img/image.png) 
![image-1.png](./img/image-1.png)

- 만약 커밋 후 푸쉬까지 해버린 상황이면?? 서로 다른 커밋으로 발생되 `push --force` 해결하기

### 우리는 커밋이용하지 말고 `stash` 기능을 사용하자 >> ✔ 불필요한 커밋을 하지 않는다!!
- ⭐ __임시 저장 공간__
![image.png](./img/image-2.png)
![image.png](./img/image-3.png)

- 저장된 스태시를 다시 적용하고 싶다면??? 
![image.png](./img/image-4.png)
- 스태시 필요하지 않다면 삭제하면 된다!
- 작업 순서
    1. `stash` 만들기
    2. 이 때 새로운 파일이 있었다면 일단 인덱스에 추가
    3. 체크아웃
    4. 되돌아 온다
    5. stash pop
    6. 보통 커밋을 새로 생성

### 주의사항
- __⭐신규파일__ >> 스태시가 안된다!! 그래서 `stage` 올려야지 stash 가능하다.

## 배치? 윈도우 자동화 배치 파일(bat)
- [자료1](https://comeinsidebox.com/bat/)
- [자료2](https://coconuts.tistory.com/358)
---
- 일괄 파일

### 배치 파일 만드는 방법 
#### `@echo off` 명령어
- 화면을 간결하게 하기 위한 명령
- 명령어의 결과만 출력
- `@` 어떤 명령어 앞에 붙이면 그 명령어 출력하지 않도록 하는 것
- `@echo off` 완벽하게 모든 명령어 구문 출력하지 않는 것
![image-7.png](./image-7.png)
![image-8.png](./image-8.png)

#### [응용] 도커 배치파일
```shell
@echo CREATE DOCKER IMAGES
cd ./testApp1
docker build -t test-app1:0.1 .
cd ./testApp2
docker build -t test-app2:0.1 .
cd ./testApp3
docker build -t test-app3:0.1 .
docker images
```

## `Over Fetching` & `Under Fatching`

### `Fetch`
- 웹 페이지 구성하기 위해 다양한 서버에 요청, 응답 데이터 받아오는 행위 > `Fetch`

### `Over Fetching`
- 필요 데이터 이상으로 서버에서 데이터를 받아오는 것

```json
{
  users : [
    {
      // 필요한 정보
      name : "zzzz",
      url : "instagram.com/zzzz",
      follower : ["aaaa", "bbbb", "cccc"],
      // 필요없는 정보
      following : ["dddd", "eeee", "ffff"],
      posts : 20,
    },
    {
      // 필요한 정보
      name : "qwer",
      url : "instagram.com/qwer",
      follower : ["asdf", "zxcv", "qaz"],
      // 필요없는 정보
      following : ["mnbv", "ljjh", "iuu"],
      posts : 10,
    }
  ]
```

### `Under Fatching`
- 한 번의 요청으로 필요한 데이터를 모두 받아오지 못해 여러 번의 요청을 수행하는 것
- 발생하는 문제점
    - 네트워크 지연
    - 느린 로딩 속도
        
## Gradle 의존성 옵션
- `implementation`
    - 의존 라이브러리 수정시 본 모듈까지만 재빌드
- `runtimeOnly`
    - __runtime__ 시에만 필요한 라이브러리인 경우
- `compileOnly`
    - __compile__ 시에만 빌드하고 빌드 결과물에는 포함하지 않음
- `annotationProcessor`
    - annotaion processor 명시
    - Annotation processing 필요없다고 예측하는 경우 빌드 제외
### 기존 그레들과의 차이점
- `compile` 지원 ❌
    - 대규모 다중 프로젝트 빌드 `api/compile` >> `implementation` 사용 시 빌드 시스템 재컴파일 해야하는 프로젝트의 크기 줄어듬 >> 빌드 시간 개선!!

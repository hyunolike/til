## 구글링 방법
> [참고 자료](https://www.youtube.com/watch?v=By_qxt0SZlI)
### 개발?
- 정확한 솔루션 검색
- 찾은 솔루션 이해하기
- 원하는대로 수정해서 적용

### 구글링
### 1. 정확한 키워드 선별
- 관련 예제, 구현 코드
- 예시 `implement drag and drop html javascript`
- ![image-9.png](./image-9.png)
- ![image-10.png](./image-10.png)

### 2. `Exact match`
- 그냥 에러 메시지같은 경우 잘나오지 않아 ㅠ,ㅠ 
- 예시 `docker "mysql exited with code 137"` >> `" "` 사용하기
- `"be of the web"`

### 3. `-Exclude`
- 특정 키워드 제외
- 예시 `store javascript date object in mysql -php` >> `-단어`

### 4. `site:url`
- 예시 `site:stackoverflow.com detect click outside ...`

### 5. `befoer:date` `after:date`
- 예시 `after:2020 검색명`

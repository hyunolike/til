## Integer vs int
### 1. int(private 자료형) 
- 자료형 의미
- 산술 연산 가능
- null 초기화 x (0 초기화 가능)
### 2. Integer(Wrapper 클래스-객체)
- Wrapper 클래스
- Unboxing x > 산술 x
- null 처리 가능
    - SQL 연동 시, 처리 용이
    - 직접직인 산술 연산 x
### 3. Boxing & Unboxing
- `Boxing`
    - Primitive Type > Wrapper class
- `Unboxing`
    - Wrapper class > Primitive
### 4. 정리
> Primitive 자료형 - Wrapper 클래스 관계

|용어|설명|
|:---:|----|
|int|primitive자료형(long, float, double ...)|
||산술 연산 가능|
||null 초기화 x|
|Integer|Wrapper클래스(객체)|
||Unboxing x > 산술 연산 x But null 처리 가능|
||null 처리 용이 > SQL과 연동 시 용이|
||__DB__ 자료형이 정수형이지만 null 값이 필요한 경우 VO에서 Integer 사용 가능|

### 5. 예시

```java
✔ to int i from Integer ii
int i = ii.intValue();

✔ to Integer ii from int i
Integer ii = new Integer(i);
```

#### valueOf() vs parseInt()
- `Integer.valueOf(String)`
    - Integer 클래스 리턴 > 산술 연산 x
- `Integer.parseInt(String)`
    - int 형 리턴 > 산술 연산 o


### 배너 생성 순서
> [참고자료](https://atoz-develop.tistory.com/entry/%EC%8A%A4%ED%94%84%EB%A7%81-%EB%B6%80%ED%8A%B8-%EB%B0%B0%EB%84%88-%EC%A0%81%EC%9A%A9%ED%95%98%EA%B8%B0-%EB%B3%80%ED%99%98-%EC%82%AC%EC%9D%B4%ED%8A%B8-%EC%B6%94%EC%B2%9C-%EB%B0%8F-color-%EB%B3%80%EA%B2%BD)
1. `resource/banner.txt` 파일 생성
2. 해당 사이트에서 배너 텍스트 만들자 >> `[배너 생성 사이트](http://patorjk.com/software/taag)
3. 해당 텍스트로 파일 생성 후 실행하기

![image-9.png](./image-9.png)


```
${AnsiColor.RED} _   .-')                     .-') _    ('-.            .-')      ('-.  _  .-')        (`-.      ('-.  _  .-')
${AnsiColor.BRIGHT_YELLOW}( '.( OO )_                  ( OO ) )  ( OO ).-.       ( OO ).  _(  OO)( \( -O )     _(OO  )_  _(  OO)( \( -O )
${AnsiColor.BRIGHT_GREEN},--.   ,--.).-'),-----. ,--./ ,--,'   / . --. /      (_)---\_)(,------.,------. ,--(_/   ,. \(,------.,------.
${AnsiColor.GREEN}|   `.'   |( OO'  .-.  '|   \ |  |\   | \-.  \       /    _ |  |  .---'|   /`. '\   \   /(__/ |  .---'|   /`. '
${AnsiColor.BRIGHT_CYAN}|         |/   |  | |  ||    \|  | ).-'-'  |  |      \  :` `.  |  |    |  /  | | \   \ /   /  |  |    |  /  | |
${AnsiColor.BLUE}|  |'.'|  |\_) |  |\|  ||  .     |/  \| |_.'  |       '..`''.)(|  '--. |  |_.' |  \   '   /, (|  '--. |  |_.' |
${AnsiColor.RED}|  |   |  |  \ |  | |  ||  |\    |    |  .-.  |      .-._)   \ |  .--' |  .  '.'   \     /__) |  .--' |  .  '.'
${AnsiColor.BRIGHT_YELLOW}|  |   |  |   `'  '-'  '|  | \   |    |  | |  |      \       / |  `---.|  |\  \     \   /     |  `---.|  |\  \
${AnsiColor.BRIGHT_GREEN}`--'   `--'     `-----' `--'  `--'    `--' `--'       `-----'  `------'`--' '--'     `-'      `------'`--' '--'
${AnsiStyle.NORMAL}
  :: ${AnsiColor.BLUE}MONA SERVER Start!! ${AnsiStyle.NORMAL} ::   ${spring-boot.formatted-version}
```

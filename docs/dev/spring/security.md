## 🍃Spring Security
> [참고 자료1](https://bcp0109.tistory.com/301)
> [참고 자료2](https://github.com/kdevkr/spring-demo-security)
> [참고 자료3](https://github.com/BAEKJungHo/spring-boot-security)
- 사용자 정보 및 유저 정보 관리 쉽게 사용할 수 있도록 제공
- __세션__ 기반 인증
    - But, `JWT` >> 토큰
- `User Role` 자체적으로 사용하는 것 같음??
    - `ROLE_USER` 정확한 형식 지켜주기
- JWT 라이브러리 설정

```groovy
// jwt 관련 의존성
implementation  group: 'io.jsonwebtoken', name: 'jjwt-api', version: '0.11.2'
runtimeOnly group: 'io.jsonwebtoken', name: 'jjwt-impl', version: '0.11.2'
runtimeOnly group: 'io.jsonwebtoken', name: 'jjwt-jackson', version: '0.11.2'
```
## 🤷‍♂️다시한번 시큐리티 정리!!
### 1. 스프링 시큐리티 아키텍처
> 2가지 영역 인증, 권한
- 인증( __Authentication__ ): 사용자에 대한 검증, 로그인된 사용자를 위한 보안전략 수립하는 것
- 권한( __Authorization__ ): 접근 제어(Access-Control)
  - 😁 스프링 시큐리티는 AOP 및 웹 요청을 __필터__ 를 통해 접근 제어 수행!!
  - 여러가지 구현체를 통해 다양한 방식으로 접근 제어 가능하다는 것으로 이해하면되

### 2. 스프링 시큐리티 시작하기
- 크게 2가지 `spring-security-web`과 `spring-security-config 모듈` 로 이루어짐
> `spring-boot-starter-security` 두 모듈에 대한 의존성 포함


```java
plugins {
    id 'org.springframework.boot' version '2.2.4.RELEASE'
    id 'io.spring.dependency-management' version '1.0.9.RELEASE'
    id 'java'
}

dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-security'
    implementation 'org.springframework.boot:spring-boot-starter-web'
}
```
- 스프링 시큐리티 자동 구성됨
### 3. 스프링 시큐리티 구성하기
#### 😎인증 매커니즘 
- 사용자 클라이언트로부터 인증 정보 모으는 것
- 사용자 정보 > 인증 매커니즘 > 인증 요청객체 > 인증관리자 > 인증 요청객체 > `SecurityContextHolder`(인증 요청객체) > 요청 시도

#### 😒구현체
- 인증 매커니즘에 대한 동작 처리할 때 사용되는 여러가지 인터페이스 및 구현체 제공

|용어|설명|
|:----:|----|
|`Authentication` `AuthenticationProvider`|인증 요청 자체적으로 하지 않고 제공자들에게 이를 위임|
|`UserDetails` `GrantedAuthority`|- `UserDetails`: 사용자의 이름, 비밀번호, 부여된 권한 그리고 사용자가 활성화되었는지 아닌지 대한 정보 제공 <br> UserDetails를 구현해 기존 사용자 정보를 인증하는데 사용 가능 !!|
|`UserDetailsService`|사용자가 입력한 정보와 비교해 인증을 확인하는 가장 일반적인 방법 제공 <br> `UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;`|
|`PasswordEncoder`|비밀번호 인코딩 처리|
|`Filter`|말 그대로 필터~~~|
|`SecurityContextPersistenceFilter`|HTTP 요청간에서 HttpSession에 포함된 속성들을 __SecurityContext__ 에 저장|
|`SecurityContextHolder`| __SecurityContext__ 를 제공하는 중요한 역할 <br> `ThreadLocal`를 사용해 정보 저장 >> 스레드에서 수행되는 메소드에서 SecurityContext를 항상 사용 가능|

![image-2.png](./image-2.png)

## ⭐시큐리티 개발 순서
### 1. User(Member) 도메인 설계
> 시큐리티 자체적으로 `UserDetail`의 구현체 User가 있기에 다른 명으로 도메인 이름 짓자
- 레포, 서비스, dto
- 애플리케이션 설정하기(아래 참고)
    - ✔ `H2 DB`, `JWT 시크릿 키` 설정
    - 원래 시크릿 키는 깃헙에 올라가지 않게 별도로 보관해야됨

```yml
spring:

  h2:
    console:
      enabled: true

  datasource:
    url: jdbc:h2:mem:testdb
    driver-class-name: org.h2.Driver
    username: sa
    password:

  jpa:
    database-platform: org.hibernate.dialect.H2Dialect
    hibernate:
      ddl-auto: create-drop
    properties:
      hibernate:
        format_sql: true
        show_sql: true

logging:
  level:
    com.tutorial: debug

# HS512 알고리즘을 사용할 것이기 때문에 512bit, 즉 64byte 이상의 secret key를 사용해야 한다.
# Secret 값은 특정 문자열을 Base64 로 인코딩한 값 사용 (아래 명령어를 터미널에 쳐보면 그대로 나옴)
# $ echo 'spring-boot-security-jwt-tutorial-jiwoon-spring-boot-security-jwt-tutorial' | base64
jwt:
  secret: c3ByaW5nLWJvb3Qtc2VjdXJpdHktand0LXR1dG9yaWFsLWppd29vbi1zcHJpbmctYm9vdC1zZWN1cml0eS1qd3QtdHV0b3JpYWwK
```
### 2. `JWT`, `Security` 설정
#### JWT
- `TokenProvider`: 유저 정보로 JWT토큰 만들거나 토큰 바탕으로 유저 정보 가져오는거 >> ⭐ JWT 토큰 관련된 암복호화, 검증 로직 모두 다 이곳에서 !!!
- `JwtFilter`: Spring Request 앞단에 붙일 Custom Filter
#### Spring Security
- `JwtSecurityConfig`: JWT Filter 추가
- `JwtAccessDeniedHandler`: 접근 권한 ❌ 403에러
- `JwtAuthenticationEntryPotin`: 인증 정보 ❌ 401에러
- `SecurityConfig`: 스프링 시큐리티에 필요한 설정
- `SecurityUtil`: SecurityContext에서 전역으로 유저 정보를 제공하는 유틸 클래스

### 3. `Refresh Token` 저장소
- Access Token, Refresh Token 함께 사용하기 때문에 필요
- 보통은 Token 만료될때 자동으로 삭제 처리하기 위해 >> `Redis` 많이 사용 

### 4. 사용자 인증 과정
- 이전까지 시큐리티, JWT 위한 설정 모두 완료!!
- `로그인 요청 > 인증 처리 > JWT 토큰 발급`
    - `AuthController` - 회원가입/로그인/재발급 api
    - `AuthService`
    - `CustomUserDetailsService` ✔

![image-1.png](./image-1.png)





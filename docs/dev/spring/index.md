# 🍃Spring
- [`mybatis` - `inner class`](https://github.com/hyunolike/info-docs/blob/main/spring/mybatis-inner-class.md)
- [`querydsl` DATE_FORMAT](https://github.com/hyunolike/info-docs/blob/main/spring/querydsl-dateformat.md)
- [`mybatis` `${}` `${}` 차이](https://github.com/hyunolike/info-docs/blob/main/spring/mybatis-%EA%B4%84%ED%98%B8%EC%B0%A8%EC%9D%B4.md)
- [⭐`OncePerRequestFilter` 특정 URL 제외시키기](https://github.com/hyunolike/info-docs/blob/main/spring/OncePerRequestFilter-notfilter.md)
- [⭐`querydsl` 상수 넣기](https://github.com/hyunolike/info-docs/blob/main/spring/querydsl-constant.md)
- [⭐`mybatis` 부등호, 비교연산자 사용 방법](https://github.com/hyunolike/info-docs/blob/main/spring/mybatis-etc.md)
- [`mybatis` LIKE문 사용](https://github.com/hyunolike/info-docs/blob/main/spring/mybatis-like.md)
- [⭐`mybatis` if 사용방법](https://github.com/hyunolike/info-docs/blob/main/spring/mybatis-if.md)
- [⭐`jpql` dto 사용](https://github.com/hyunolike/info-docs/blob/main/spring/jpql-dto.md)
- [⭐`mybatis` dto로 리턴값 보내는 방법](https://github.com/hyunolike/info-docs/blob/main/spring/mybatis-dto-result.md)
- [JWT](https://github.com/hyunolike/info-docs/blob/main/spring/jwt.md)
- [Spring Session & JWT](https://github.com/hyunolike/info-docs/blob/main/spring/jwt-springsecurity.md)
- [JPA 원하는 내용만 가져오는 방법](https://github.com/hyunolike/info-docs/blob/main/spring/jpa1.md)
- [마이크로서비스](https://wonit.tistory.com/506)
- [Jsch](https://github.com/hyunolike/info-docs/blob/main/spring/jsch.md)
- [Spring JPA `insertable` `updatable`](https://github.com/hyunolike/info-docs/blob/main/spring/jpa_insertable_updatable.md)
- [Spring JPA 식별자 할당 `SEQUENCE(시퀀스)` 사용 전략](https://github.com/hyunolike/info-docs/blob/main/spring/jpa_sequence.md)
- [스프링에서 오라클 시퀀스 사용하는 방법 + JPA](https://github.com/hyunolike/info-docs/blob/main/spring/spring_jpa_sequence.md)
- [JPA - `CriteriaQuery`](https://github.com/hyunolike/info-docs/blob/main/spring/jpa_criteriaQuery.md)
- [`StringUtils`](https://github.com/hyunolike/info-docs/blob/main/spring/StringUtils.md)
- [JPA - `Specification`](https://github.com/hyunolike/info-docs/blob/main/spring/Specification.md)
- [스프링 커스텀 예외처리](https://github.com/hyunolike/info-docs/blob/main/spring/custion_exception.md)
- [서비스 장애 대비한 `Log` 남기기](https://github.com/hyunolike/info-docs/blob/main/spring/log.md)
- [마리아 디비 외래키 설정후 JPA 설정 방법 - sql 파일 + JPA + 뷰 테이블](#)
- [테스트 코드?? TDD에 대해](#)
- [`PropertySourcesPlaceholderConfigurer` properties 파일값 읽어오기](https://github.com/hyunolike/info-docs/blob/main/spring/PropertySourcesPlaceholderConfigurer.md)
- [`JPQL`](https://github.com/hyunolike/info-docs/blob/main/spring/jpa_jpql.md)
- [`JPA`에서 Query 사용하는 방법](https://github.com/hyunolike/info-docs/blob/main/spring/jpa-query.md)
- [`properterties` 속성값 가져와서 사용하자](https://github.com/hyunolike/info-docs/blob/main/spring/properties-spring.md)
- [`JPA` `Hibernate` `Spring Data JPA`](https://github.com/hyunolike/info-docs/blob/main/spring/jpa-hibernate-springdatajpa.md)
- [`MS SQL` `JPA` 연결 및 설정](https://ingus26.tistory.com/28)
- [springboot 외부 라이브러리 jar 파일 추가 방법](https://github.com/hyunolike/info-docs/blob/main/spring/libs-jar.md)
- [`JUnit`](https://github.com/hyunolike/info-docs/blob/main/spring/Junit.md)
- [`JPA` `@Modifying`](https://github.com/hyunolike/info-docs/blob/main/spring/jpa-modifying.md)
- [`JPA` `@Prepersist`](https://github.com/hyunolike/info-docs/blob/main/spring/prepersist.md)
- [배포 `jar` `war`](https://github.com/hyunolike/info-docs/blob/main/spring/jar-war.md)
- [스프링 `Transactional`](https://github.com/hyunolike/info-docs/blob/main/spring/transaction-spring.md)
- [단위 테스트, 통합 테스트, 인수 테스트](https://github.com/hyunolike/info-docs/blob/main/spring/tdd.md)
- [`logback`](https://github.com/hyunolike/info-docs/blob/main/spring/logback.md)
- [Spring `profiles`](https://github.com/hyunolike/info-docs/blob/main/spring/profiles.md)
- [`logback` 파일 생성하기](https://github.com/hyunolike/info-docs/blob/main/spring/logback-file.md)
- [`logback` - `RollingFileAppendeer`](https://github.com/hyunolike/info-docs/blob/main/spring/logback-rollingfileappender.md)
- [배포 환경별 설정](https://github.com/hyunolike/info-docs/blob/main/spring/spring-profile-logging.md)
- [`Intellij` 외부 jar(lib/.jar) 파일 추가](https://github.com/hyunolike/info-docs/blob/main/spring/intellij-jar-add.md)
- [Gradle task](https://github.com/hyunolike/info-docs/blob/main/spring/gradle-task.md)
- [`jar` 빌드](https://github.com/hyunolike/info-docs/blob/main/spring/jar-build.md)
- [`Gradle`](https://github.com/hyunolike/info-docs/blob/main/spring/gradle-info.md)
- [`MockMvc`](https://github.com/hyunolike/info-docs/blob/main/spring/mockmvc.md)
- [`logback` - rollingPolicy](https://github.com/hyunolike/info-docs/blob/main/spring/logback-rollingpolicy.md)
- [예외 처리 개발](https://github.com/hyunolike/info-docs/blob/main/spring/exception-dev.md)
- [`SpringMVC`에서 Ajax, JSON](https://github.com/hyunolike/info-docs/blob/main/spring/springmvc-json-ajax.md)
- [`@ResponseEntity` vs `@ResponseBody`](https://github.com/hyunolike/info-docs/blob/main/spring/responsebody-entity.md)
- [`Spring Security`](https://github.com/hyunolike/info-docs/blob/main/spring/spring-security.md)
- [외부 프로퍼티](https://github.com/hyunolike/info-docs/blob/main/spring/springboot-properties-file.md)
- [`Profile` 개념](https://github.com/hyunolike/info-docs/blob/main/spring/spring-profile.md)
- [Spring - `Exception` 전략](https://github.com/hyunolike/info-docs/blob/main/spring/exception-strategy.md)
- [Controller - `@Valid` `@Validated` 값 검증](https://github.com/hyunolike/info-docs/blob/main/spring/controller-valid.md)
- [`@MockMvc` vs `@SpringBootTest`](https://github.com/hyunolike/info-docs/blob/main/spring/mockmvc-springboottest.md)
- [`PropertySourcesPlaceholderConfigurer` > 프로퍼티 지정](#)
- [`Gradle` - 테스트 유무에 따른 빌드 명령어](https://github.com/hyunolike/info-docs/blob/main/spring/gradle-build.md)
- [`Gradle` - `gradle` vs `gradlew`](https://github.com/hyunolike/info-docs/blob/main/spring/gradle-gradlew.md)
- [`STOMP` + `WebSocket`](https://github.com/hyunolike/info-docs/blob/main/spring/stomp.md)
- [`JPA` - 더티 채킹](https://github.com/hyunolike/info-docs/blob/main/java/dirty-checking.md)
- [`WebMvcConfigurer`](https://github.com/hyunolike/info-docs/blob/main/spring/WebMvcConfigurer.md)
- [`HandlerInterceptor`](https://github.com/hyunolike/info-docs/blob/main/spring/HandlerInterceptor.md)
- [`Spring Framework Module`](https://github.com/hyunolike/info-docs/blob/main/spring/spring-framework-module.md)
- [Spring boot - `Graceful Shutdown`](https://github.com/hyunolike/info-docs/blob/main/spring/graceful.md)
- [`JPA` - 벌크 연산(Bulk Operation)](https://github.com/hyunolike/info-docs/blob/main/spring/bulk-operation.md)
- [Gradle Wrapper](https://github.com/hyunolike/info-docs/blob/main/spring/gradle-wrapper.md)
- [Spring 동기, 비동기, 배치 처리 >> `context` 유지 및 로깅 - `mdc` `threadlocal`](https://github.com/hyunolike/info-docs/blob/main/spring/threadloacl.md)
- [Spring` 템플릿 콜백 패턴](https://github.com/hyunolike/info-docs/blob/main/spring/template-callback.md)
- [`@NoargsConstructor(AccessLevel.PROTECTED)` 와 `@Builder`](https://github.com/hyunolike/info-docs/blob/main/spring/accesslevel-protected.md)
- [`JPA Auditing` - `CreateBy` `UpdateBy`](https://github.com/hyunolike/info-docs/blob/main/spring/jpa-createby-updateby.md)
- [`@Transaction(readOnly=true)`](https://github.com/hyunolike/info-docs/blob/main/spring/transaction.md)
- [`Artifact` `Group`](https://github.com/hyunolike/info-docs/blob/main/spring/artifact.md)
- [Parse Snake case JSON in Spring Boot >> DTO를 snake case로 바꿔주자](https://github.com/hyunolike/info-docs/blob/main/spring/snake-case.md)
- [`@JsonIgnore` `@JsonIgnoreProperties` `@JsonIgnoreType`](https://github.com/hyunolike/info-docs/blob/main/spring/jsonignore.md)
- [`logback` `logging` 간단 설정](https://github.com/hyunolike/info-docs/blob/main/spring/log_%EA%B0%84%EB%8B%A8%EC%84%A4%EC%A0%95.md)
- [`@Transactional` 옵션](https://github.com/hyunolike/info-docs/blob/main/spring/transactional.md)
- [`Spring AOP` - logging](https://github.com/hyunolike/info-docs/blob/main/spring/aop-logging.md)
- [Gradle Build Lifecycle](https://github.com/hyunolike/info-docs/blob/main/spring/gradle-build-lifecycle.md)
- [Spring Bean 등록 - 중복 코드를 줄여보자](https://github.com/hyunolike/info-docs/blob/main/spring/spring-bean.md)
- [Spring Bean Scope](https://github.com/hyunolike/info-docs/blob/main/spring/bean-scope.md)
- [`@NotNull` `@NotEmpty` `@NotBlank`](https://github.com/hyunolike/info-docs/blob/main/spring/notnull-notempty-notblank.md)
- [프록시와 내부호출](https://github.com/hyunolike/info-docs/blob/main/spring/proxy-aop.md)
- [`Gradle Kotiln DSL` - 우아한형제들](https://github.com/hyunolike/info-docs/blob/main/spring/gradle-kotlin.md) - [참고자료](https://techblog.woowahan.com/2625/)
- [⭐테스트 범위와 종류](https://github.com/hyunolike/info-docs/blob/main/spring/test-range.md)
- [`DataAccessException`](https://github.com/hyunolike/info-docs/blob/main/spring/dataaccessexception.md)
- [원인 예외 `cause exception`](https://github.com/hyunolike/info-docs/blob/main/spring/cause-exception.md)
- [react & spring에서 `CORS` 설정 방법](https://github.com/hyunolike/info-docs/blob/main/spring/spring-react-cors.md)
- [⭐`Interceptor` 사용해 인증 구현 - `Spring Security ❌`](https://github.com/hyunolike/info-docs/blob/main/spring/interceptor.md)
- [⭐ `JPA` 외래키 - `@JoinColumn`](https://github.com/hyunolike/info-docs/blob/main/spring/jpa-joincolumn.md)
- [JPA - 영속](https://github.com/hyunolike/info-docs/blob/main/spring/jpa-cascade.md)
- [Dialect 방언 - JPA](https://github.com/hyunolike/info-docs/blob/main/spring/dialect.md)
0 [`Filter` vs `Interceptor`](https://github.com/hyunolike/info-docs/blob/main/spring/filter-interceptor.md)
- [`J2EE`](https://github.com/hyunolike/info-docs/blob/main/spring/j2ee.md)
- [`JPA` mappedby 이해하기](https://github.com/hyunolike/info-docs/blob/main/spring/jpa-mappedby.md)
- [`MVC` vs `WebFlux`](https://github.com/hyunolike/info-docs/blob/main/spring/mvc-webflux.md)
- [⭐`N+1` 문제](https://github.com/hyunolike/info-docs/blob/main/spring/n_1.md)
- [`OSIV` Open Session In View](https://github.com/hyunolike/info-docs/blob/main/spring/osiv.md)
- [⭐`Dispatcher Servlet`](https://github.com/hyunolike/info-docs/blob/main/spring/dispatcher-servlet.md)
- [jpa 영속성 컨텍스트](https://github.com/hyunolike/info-docs/blob/main/spring/persistance-context.md)
- [`jsp` vs `thymeleaf`](https://github.com/hyunolike/info-docs/blob/main/spring/jsp-thymeleaf.md)
- [`jsp` 데이터 전달](https://github.com/hyunolike/info-docs/blob/main/spring/jsp-data.md)
- [⭐`jsp` 데이터 프런트, 백 전달 방법](https://github.com/hyunolike/info-docs/blob/main/spring/jsp-value-input-output.md)
- [⭐️`spring component scan`](https://github.com/hyunolike/info-docs/blob/main/spring/component-scan.md)
- [⭐`Spring Security` 세션기반, 토큰기반](https://github.com/hyunolike/info-docs/blob/main/spring/spring-security-session-token.md)
- [⭐스프링 요청 처리 구조](https://github.com/hyunolike/info-docs/blob/main/spring/%EC%9A%94%EC%B2%AD%EC%B2%98%EB%A6%AC%EA%B5%AC%EC%A1%B0.md)
- [Apache Tomcat](https://github.com/hyunolike/info-docs/blob/main/spring/tomcat.md)
- [`@EqualsAndHashCode` `@Data`](https://github.com/hyunolike/info-docs/blob/main/spring/lombok-equalsandhashcode.md)
- [`maven` .m2 폴더란](https://github.com/hyunolike/info-docs/blob/main/spring/mavne-m2.md)
- [`spring data envers` 데이터 변경 이력(히스토리) 관리](https://github.com/hyunolike/info-docs/blob/main/spring/spring-envers.md)
- [`spring event`](https://github.com/hyunolike/info-docs/blob/main/spring/spring-event.md)
- [`Spring Aop` & `AspectJ`](https://github.com/hyunolike/info-docs/blob/main/spring/springaop-aspectj.md)
- [application context](https://github.com/hyunolike/info-docs/blob/main/spring/application-context.md)
- [spring aop 정리](https://github.com/hyunolike/info-docs/blob/main/spring/spring-aop.md)
- [spring scheduler](https://github.com/hyunolike/info-docs/blob/main/spring/spring-scheduler.md)
- [QueryDSL](https://github.com/hyunolike/info-docs/blob/main/spring/querydsl.md)
- [⭐DTO 전략](https://github.com/hyunolike/info-docs/blob/main/spring/dto-entity.md)
- [`QueryDSL` @QueryProjection](https://github.com/hyunolike/info-docs/blob/main/spring/querydsl-projection.md)
- [우아한 형제들 `QueryDSL` 사용법](https://github.com/hyunolike/info-docs/blob/main/spring/woo-querydsl.md)
- [`@ColumnDefault` 칼럼 default 적용](https://github.com/hyunolike/info-docs/blob/main/spring/column-default.md)
- [`spring boot` + `JUnit5` 테스트 초기 데이터 로딩](https://github.com/hyunolike/info-docs/blob/main/spring/junit5-dummy-data.md)
- [⭐`Fetch Join`](https://github.com/hyunolike/info-docs/blob/main/spring/fetch-join.md)
- [⭐`test` - Mockito](https://github.com/hyunolike/info-docs/blob/main/spring/test-mockito.md)
- [`spring test` - ReflectionTestUtils](https://github.com/hyunolike/info-docs/blob/main/spring/reflectiontestutils.md)
- [`spring-test` `MockMvc` webAppContextSetup](https://github.com/hyunolike/info-docs/blob/main/spring/spring-test-webAppContextSetup.md)
- [`spring-test` MockHttpServletRequestBuilder](https://github.com/hyunolike/info-docs/blob/main/spring/MockHttpServletRequestBuilder.md)
- [⭐자바 메서드 네이밍](https://github.com/hyunolike/info-docs/blob/main/spring/java-method-naming.md)
- [매직 넘버, 매직 리터럴](https://github.com/hyunolike/info-docs/blob/main/spring/%EB%A7%A4%EC%A7%81%EB%84%98%EB%B2%84_%EB%A7%A4%EC%A7%81%EB%A6%AC%ED%84%B0%EB%9F%B4.md)
- [`@ConfigurationProperties`](https://github.com/hyunolike/info-docs/blob/main/spring/configuration.md)
- [`Class<T>` - 리플렉션](https://github.com/hyunolike/info-docs/edit/main/spring/class-reflection.md)
- [⭐`@Primary` `@Qualifier`](https://github.com/hyunolike/info-docs/blob/main/spring/qualifier-primary.md)
- [⭐스레드 동기화 `Synchronized`](https://github.com/hyunolike/info-docs/blob/main/spring/synchronized.md)
- [@SuppressWarnings](https://github.com/hyunolike/info-docs/blob/main/spring/suppresswarnings.md)
- [엑셀 라이브러리 - `POI`](https://github.com/hyunolike/info-docs/blob/main/spring/java-excel-lib.md)
- [`Spring batch` JdbcCursorItemReader](https://github.com/hyunolike/info-docs/blob/main/spring/JdbcCursorItemReader.md)
- [⭐JPA null 필드 제외 응답하기](https://github.com/hyunolike/info-docs/blob/main/spring/jsoninclude-non-null.md)
- [Spring Boot + `SnakeYaml` yaml 파일 조작 라이브러리 (추가. YAML 소개)](https://github.com/hyunolike/info-docs/tree/main/spring)
- [Filter](https://github.com/hyunolike/info-docs/blob/main/spring/filter.md)
- [⭐spirng singleton container](https://github.com/hyunolike/info-docs/blob/main/spring/spring-singleton.md)
- [sourceCompatibility](https://github.com/hyunolike/info-docs/blob/main/spring/sourceCompatibility.md)
- [`@ColumnDefault` `@Builder.Default` 차이](https://github.com/hyunolike/info-docs/blob/main/spring/columndefault-builderdefault.md)
- [Spring 필터, 인터셉터](https://github.com/hyunolike/info-docs/blob/main/spring/filter-interceptor-docs.md)
- [`Spring Security` 사용자 비밀번호 검사](https://github.com/hyunolike/info-docs/blob/main/spring/security-pw-check.md)
- [`OncePerRequestFilter`](https://github.com/hyunolike/info-docs/blob/main/spring/oncePerRequestFilter.md)
- [⭐SecurityContextHolder](https://github.com/hyunolike/info-docs/blob/main/spring/securitycontextholder.md)
- [⭐HandlerInterceptor](https://github.com/hyunolike/info-docs/blob/main/spring/handler-interceptor.md)
- [⭐뷰테이블 + `JPA` 엔티티](https://github.com/hyunolike/info-docs/blob/main/spring/viewtable-entity.md)
- [⭐인터셉터, 필터, AOP 차이](https://github.com/hyunolike/info-docs/blob/main/spring/interceptor-filter-aop.md)
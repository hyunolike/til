## Response 응답 종류 및 방법

### `@RestController`

```java
@RestController ✔ 
public class ApiController {
    // TEXT
    @GetMapping("/text")
    public String text(@RequestParam String account) {
        return account;
    }

    // JSON
    // req > object mapper > object > method > object > object mapper > json > response
    @PostMapping("/json")
    public User json(@RequestBody User user){
        return user;
    }

    // 명확하게 응답하는 방법
    // ResponseEntity ✔
    @PutMapping("/put")
    public ResponseEntity<User> put(@RequestBody User user){
        return ResponseEntity.status(HttpStatus.CREATED).body(user); // 201코드
    }
}
```

### `@Controller`

```java
@Controller // html 리소스 찾는거야 ✔
public class PageController {
    @RequestMapping("/main")
    public String main(){
        return "main.html";
    }

    // ReponseEntity

    @ResponseBody ✔
    @GetMapping("/user")
    public User user() {
        User user = new User();
        user.setName("hyunho");
        user.setAddress("강남구");
        return user;
    }
}
```

### JSON null 처리
- 해당 `DTO` 상단에 `@JsonInclude(JsonInclude.Include.NON_NULL)`

### JSON int형 null 값 처리

```java
int age // null
Integer age // ""
```
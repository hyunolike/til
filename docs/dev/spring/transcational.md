## `@Transactional`
> [참고자료](https://velog.io/@kdhyo/JavaTransactional-Annotation-%EC%95%8C%EA%B3%A0-%EC%93%B0%EC%9E%90-26her30h)

### 트랜잭션
- 데이터베이스의 상태를 변경하는 작업 / 한번에 수행되어야 하는 연산들
- `begin` `commit` 자동으로 수행
- 예외 발생 시 `rollback` 자동으로 수행
- rollback 하고 싶으면 `@Transcational(rollbackFor = Exception.class)`

### 4가지 성질
- 원자성
    - 실행한 작업들은 하나의 단위로 처리
- 일관성
    - 일관성있는 데이터베이스 상태 유지
- 격리성
    - 동시에 실행되는 트랜잭션드리 서로 영향 미치지 않음
- 영속성
    - 트랜잭션을 성공적으로 마치면 결과가 항상 저장

### 트랜잭션 처리 방법
- `선언적 트랜잭션` : 🍃스프링 >> `@Transactional` 메소드, 클래스, 인터페이스 위에 추가하는 방식 

```java
@Transactional
public void addUser(UserDTO dto) throws Exception {
	// 로직 구현
}
```

### `@Transactional` 옵션
- `isolation`: 일관성없는 데이터 허용 수준 설정
- `propagation`: 트랜잭션 동작 도중 다른 트랜잭션 호출 시, 어떻게 할 것인지 저장하는 옵션
- `noRollbackFor`: 특정 예외 발생 시 rollback x
- `rollbackFor`: 특정 예외 발생 시 rollback
- `timeout`: 지정한 시간 내에 매소드 수행 완료되지 않으면 rollback
- `readOnly`: 트랜잭션 읽기 전용

### ✔ 왜 (rollbackFor = Exception.class) 해??
- `@Transactional` 기본적으로 __Unchecked Exception, Error__ 만을 롤백
- 따라서, 모든 예외에 대해서 롤백 진행하고 싶으면 `(rollbackFor = Exception.class)` 붙이자
    - 그 이유는 Checked Exception 같은 경우는 예상된 에러이며
Unchecked Exception, Error 같은 경우는 예상치 못한 에러이기 때문이란 것을 알게 되었다.

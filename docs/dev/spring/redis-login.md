## Spring 에서 Redis DB 왜 사용?? - [참고자료](https://github.com/woowacourse-teams/2021-botobo/wiki/%EB%A1%9C%EA%B7%B8%EC%9D%B8,-%EB%A1%9C%EA%B7%B8%EC%95%84%EC%9B%83-%ED%9D%90%EB%A6%84-%EC%A0%95%EB%A6%AC)
> 여기서 `Spring Security` `JWT` `Redis DB` 
- JWT: Access Token만 관리할 때는 사용 x / Access Token + Refresh Token 도 관리할 때 사용 o

### 1. 유저가 로그인 시도
- ![image-23.png](./image-23.png)
- ![image-24.png](./image-24.png)

### 2. 디비에서 관리하는 이유
- 해당 진행하는 프로젝트내 에서
    - RefreshToken이 만료되지 않았음에도 새로운 환경에서 로그인 시도 시 **재발급하는 구조** !! 이기에
    - 따라서, 유저 한 명당 가장 최근에 발급한 하나의 RefreshToken만 Redis 저장하는 구조
    - ✅ 즉, 어떤 유저가 RefreshToken이 redis에 있는데 새로운 환경에서 로그인 하면 그 때 새로 발급 받은 RefreshToken으로 대체

### 3. 로그아웃 시도
- 기존 AccessToken만 있을 때에는 로그아웃 api 필요 없음!!! 프런트에서 안 보내면 그만 !!
- 아래 표 참고 (RefreshToken 존재 시)


|RefreshToken ⭕️|RefreshToken ❌|
|------|-----|
|- redis에서 refreshtoken 삭제<br>- 유저 쿠키 set-cookie(쿠키 안에 refreshtoken 없고, 쿠키의 MaxAge = 0)<br>- `no-content` 응답|- 유저 쿠키 set-cookie(쿠키 안에 refreshtoken 없고, 쿠키의 MaxAge = 0)<br>- `no-content` 응답|

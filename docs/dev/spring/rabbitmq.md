## RabbitMQ
> [RabbitMQ 실습 자료](https://oingdaddy.tistory.com/165)
- [메시지 큐](https://blog.naver.com/PostView.nhn?blogId=dktmrorl&logNo=222117711303&categoryNo=0&parentCategoryNo=0&viewDate=&currentPage=1&postListTopCurrentPage=1&from=postView)
- 메세지 큐 시스템
- 얼랭(Erlang)으로 `AMQP`를 구현한 메시지 브로커 시스템 >> 따라서 RabbitMQ 설치 시 해당 얼랭언어 설치해야됨!!!
    - 얼랭: 범용 병렬 프로그래밍 언어
    - `AMQP`(Advanced Message Queuing Protocal): 메시지 지향 미들웨어를 위한 개방형 표준 응용 계층 프로토콜


![image-4.png](./image-4.png)


### EXCHANGE 모드
    - `Direct`: bindKey = exchange 같은 큐에 전달 
    - `Topic` : bindKey, exchange 일부 패턴이 같은 큐에 전달
    - `Fanout`: 전체 전달

### Virtual host
- 가상 호스트로 사용자마다 가상 호스트에 권한 부여 >> 권한 있는 사용자만 접근 가능하도록!!







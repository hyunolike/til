## `RabbitMQ` 실습해보자
### 1. 사전 작업
- `Exchange`
- `Queue`
- `Rouing key`
- 위 3가지를 RabbitMQ Management에서 생성
    - 유저/비밀번호 > `guest`
    - 주소 > `http://localhost:15672/`

### 1. 스프링 프로젝트 2개 생성!
> 역할에 따라 `Publisher` `Consumer`
- 의존성
    - Spring Web
    - Spring for RabbitMQ 

#### 먼저 `Consumer` 프로젝트 
- Queue에 있는 정보를 받아주기 위한 `Listener` 생성!


![image-5.png](./image-5.png)
#### 다음으로 `Publisher` 프로젝트
- `exchange` `queue` `routing` 정보 빈 설정

### 2. 구조
![image-6.png](./image-6.png)

### 팁
> [자료](https://minholee93.tistory.com/entry/RabbitMQ-JSON-Message-Format-%EC%82%AC%EC%9A%A9%ED%95%98%EA%B8%B0-1)
- 만약 `String` 형식이 아닌 `JSON` 형식을 보내고 싶으면
- `Publisher` 쪽 직렬화를 통해 
- `Consumer` 쪽 역직렬화를 통해
- 서로 주고 받으면 된다!

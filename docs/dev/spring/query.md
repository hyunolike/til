## `@Query`
> [참고 링크](https://wonit.tistory.com/470)

### JPA 제공하지 않는 기능들은 어떻게 구현?
- `사용자 정의 쿼리` 를 사용!
    1. `Named Query`
    2. `쿼리 메서드`
    3. `@Query` 

### `@Query` 
- 개발자가 원하는 쿼리 직접 짜야할 때 아주 강력
- 해당 쿼리는 `JQPL` 사용

### `JQPL`
- `Java Persistence Query Language`
    - `SQL`: __테이블__ 대상으로 쿼리
    - `JPQL`: __엔티티 객체__ 대상으로 쿼리
- 사용: `참조변수.필드`

```sql
select
    m.username,
    m.address
from Member m
where
    m.age > 18
```
- ⭐ 테이블 대상으로 쿼리 날리는 것이 아닌 __엔티티__ 대상으로 쿼리를 날림!

### 파라미터 바인딩 시키기
- 사용자 정의 쿼리를 하는 것

```java
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("쿼리문")
    List<User> methodName();
```
- 2가지 방법 (되도록 __이름 기반__ 사용하자)
    - 위치 기반 ❌  
    - 이름 기반 ✔

```java
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("select u from User u where u.username = :name")
    List<User> methodName(@Param("name") String username);
```

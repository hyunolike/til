## 매개변수를 넘겨받는 방법
> controller

### `@PathValue`
- `/{id}` 와 같은 url의 경로로 넘겨오는 값을 변수로 받을 수 있다! 

```java
@GetMapping("/{id}")
public String testControllerWithPathVariables(@PathVariable(required=false) int id){
    return "Hyunho의 id >>" + id;
}
```
- `http://localhost:8080/123` 이렇게 요청하면 결과는 __Hyunho의 id >> 123__ 이렇게 출력
- 여기서, `required=false` 꼭 매개변수가 필요한 것은 아니라는 뜻

### `@RequestParam`
- `?id={id}` 

```java
@GetMapping("/requestParam")
public String testControllerWithPathVariables(@RequestParam(required=false) int id){
    return "Hyunho의 id >>" + id;
}
```
- `http://localhost:8080/requestParam?id=123` 

### `@RequestBody`
- 보통 반환하고자 하는 리소스가 복잡할 때 사용!!

`DTO`
```java
@Data
public class Dto(){
    private int id;
    private String message;
}
```

`CONTROLLER`
```java
...
@GetMappling("/requestBody")
public String testControllerWithRequestBody(@RequestBody Dto dto){
    return " 나의 id는 " + dto.getId() + "나의 메시지는!!! " + dto.getMessage();
}
```

`결과`
```json
{
    "id": 123,
    "message": "나는 개발자 :)"
}
```

### `@ResponseBody`
> `@RestController` = @Controller + @ResponseBody ✌🏻
- 기존까지 문자열 하지만 더 복잡한거는?? 오브젝트 같은거 리턴
- 요청을 통해 가져올순 있지만 응답으로 리턴할 수 있다 !!
    - 간단 그 자체 !!!
- 결과는 `JSON` 으로 반환

```java
@GetMapping("/responseBody")
public ObjectDto<String> testControllerWithResponseBody() {
    List<String> list = new ArrayList<>();
    list.add("이것은 dto 반환만 하면 끝!! 간단하지?");
    ObjectDto<String> response = ObjectDto.<String>builder().data(list).build();
    return response;
}
```

### `ResponseEntity`
- `status` `header` 조작하고 싶을 때 사용해!!

```java
@GetMapping("/responseEntity")
public ObjectDto<String> testControllerWithResponseEntity() {
    List<String> list = new ArrayList<>();
    list.add("이것은 dto 반환만 하면 끝!! 간단하지?");
    ObjectDto<String> response = ObjectDto.<String>builder().data(list).build();
    return ResponseEntity.badRequest().body(response); // http status 400 으로 설정 ✅
}
```


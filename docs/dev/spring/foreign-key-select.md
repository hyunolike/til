## 스프링 부트에서 외래키로 데이터 조회 방법
> [스택오버플로우 자료](https://stackoverflow.com/questions/56872364/how-pass-a-parameter-for-a-foreign-key-in-a-jpa-query-when-its-an-object)


```java
@org.springframework.stereotype.Repository
public interface Repository extends JpaRepository<Channel, Long> {
    @Query(value = "SELECT c FROM Channel c WHERE c.user1.userId = :id")
    List<Channel> findByUser1(@Param("id") String userId);
}
```
### 1. domain
`User.java`


```java
package com.mona.backend.user.domain;

import com.mona.backend.common.entity.BaseEntity;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "user")
public class User extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id", nullable = false, unique = true, length = 80)
    private String userId;

    @Column(nullable = false, unique = true, length = 80)
    private String nickname;

    @Column(name = "affine_x") // TODO: 제약 조건 추가
    private String affineX;

    @Column(name = "affine_y")
    private String affineY;

    @Builder
    public User(String userId, String affineX, String affineY, String nickname) {
        this.userId = userId;
        this.affineX = affineX;
        this.affineY = affineY;
        this.nickname = nickname;
    }

    public void update(String affineX, String affineY){
        this.affineX = affineX;
        this.affineY = affineY;
    }
}

```

- `Channel.java`


```java
package com.mona.backend.channel.domain;

import com.mona.backend.user.domain.User;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "channel")
public class Channel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "channel_url", nullable = false, length = 254)
    private String channelUrl;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id1")
    private User user1;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id2")
    private User user2;

    @Column(name = "random_number", length = 254)
    private String randomNumber;

    @Builder
    public Channel(User user1,User user2, String channelUrl, int numberCount, String randomNumber) {
        this.user1 = user1;
        this.user2 = user2;
        this.channelUrl = channelUrl;
        this.randomNumber = randomNumber;
    }
}

```

### 2. erd
- ![image-22.png](./image-22.png)

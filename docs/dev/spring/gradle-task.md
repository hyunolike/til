## Gradle `Junit` 단위 / 통합 테스트 분류 및 `Gradle Task` 설정
> [참고자료](https://geminikim.medium.com/%ED%85%8C%EC%8A%A4%ED%8A%B8-%EC%9E%90%EB%8F%99%ED%99%94-junit-test-%EB%B6%84%EB%A6%AC-%EB%B0%8F-gradle-%EC%84%A4%EC%A0%95-f1e28d4d19a4)

### 1. @Category(Integration.class)
```java
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Category(IntegrationTest.class)
public abstract class IntegrationTest {
}
```

#### 1.1 상속 여부로 구분 
```java
//단위 테스트 작성 시..
public class DealFinderTest {
  {...}
}

//통합 테스트 작성 시..
public class UsableOptionDslRepositoryIntegrationTest extends IntegrationTest {
  {...}
}
```

### 2. build.gradle 설정
```groovy
//full source -> https://github.com/geminiKim/minimerce/blob/master/build.gradle
/**** Test Tasks ****/
test {
	useJUnit {
		excludeCategories 'com.minimerce.IntegrationTest'
	}
}
task integrationTest(type: Test, group: 'verification') {
	useJUnit {
		includeCategories 'com.minimerce.IntegrationTest'
	}
}
task wholeTest(type: Test, group: 'verification') { 
	useJUnit { } 
}
```

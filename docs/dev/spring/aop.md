## AOP 관점지향 프로그래밍 
- 로직 기준 > 핵심적인 관점, 부가적인 관점 > `각각 모듈화`
    - 핵심 비즈니스 로직

### AOP 주요 개념
- `Aspect`
    - 관심사 모듈화(주로 부가기능 모듈화함)
- `Target`
    - Aspect 적용하는 곳(클래스, 메서드 ...)
- `Advice`
    - 실질적으로 어떤일 해야하는지 / 실질적인 부가기능 담은 구현체
- `JoinPoint`  
    - Advice가 적용될 위치
- `PointCut` 
    - JoinPoint의 상세한 스펙 정의

### PointCut 표현식 정리

|용어|설명|
|----|----|
|`execution`|Advice 적용할 메서드 명시|
|`within`|특정 타입에 속하는 메서드 > JoinPoint로 설정되도록 명시|
|`bean`|스프링 2.5버전부터 지원 > 스프링 빈을 이용해 JoinPoint 설정|

#### `excution` 명시자
- `execution([수식어] 리턴타입[클래스이름].이름(파라미터))`
    - 수식어: public, private 등 수식어 명시(생략 가능)
    - 리턴타입: 리턴타입 명시
    - 클래스 이름 및 이름: 클래스이름과 메서드 이름 명시(클래스 이름 >> 풀 패키지명 명시 / 생략 가능)
    - 파라미터: 메서드의 파라미터 명시
    - `*`: 모든 값 표현
    - `..`: 0개 이상 표현

`execution(public Integer com.edu.aop.*.*(*))`

 - com.edu.aop 패키지에 속해있고, 파라미터가 1개인 모든 메서드

 

`execution(* com.edu..*.get*(..))`

 - com.edu 패키지 및 하위 패키지에 속해있고, 이름이 get으로 시작하는 파라미터가 0개 이상인 모든 메서드 

 

`execution(* com.edu.aop..*Service.*(..))`

 - com.edu.aop 패키지 및 하위 패키지에 속해있고, 이름이 Service르 끝나는 인터페이스의 파라미터가 0개 이상인 모든 메서드

 

`execution(* com.edu.aop.BoardService.*(..))`

 - com.edu.aop.BoardService 인터페이스에 속한 파마리터가 0개 이상인 모든 메서드

 

`execution(* some*(*, *))`

 - 메서드 이름이 some으로 시작하고 파라미터가 2개인 모든 메서드

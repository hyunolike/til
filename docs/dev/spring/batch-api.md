## `batch api` 리턴값
- ✔ 예외처리 발생 안된 부분까지 성공 데이터 추가
- ✔ 예외처리 발생 시 예외 데이터 추가를 통해 파악 가능
![image-18.png](./image-18.png)
![image-19.png](./image-19.png)
![image-20.png](./image-20.png)
---
### 개선된 리턴값
![image-21.png](./image-21.png)

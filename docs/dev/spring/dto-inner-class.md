## DTO Inner Class

### 기존 문제점
- `API` 별로 return하는 데이터가 달라 > 많은 `DTO` 생성
- `Entity` 들의 데이터들을 가공 > DTO의 `setMethod` `builder` 매핑하는 코드가 길어짐

### `Inner Class`를 깔끔하게 관리하기
- __예시__
    1. POST API > Request Payload 매핑할 DTO
    2. GET API > Return 해줄 Response DTO
    3. 레이어 옮겨다니며 결과를 Return하기 위해 실제 User 정보 담은 DTO
#### 🤣 벌써 DTO가 3개?? 개선해보자

`User.class` 
```java
public class User{
    @Getter
    @AllArgsConstructor
    @Builder
    public static class Info {
        private int id;
        private String name;
        private int age;
    }

    @Getter 
    @Setter
    public static class Request {
        private String name;
        private int age;
    }

    @Getter
    @AllArgsConstructor
    public static class Response {
        private Info info;
        private int returnCode;
        private String returnMessage;
    }
}
```

`UserController.class`
```java
@RestController
@RequestMapping("user")
public class UserController {

    @GetMapping("/{user_id}")
    public User.Response getUser(@PathVariable("user_id") String userId) {
        return new User.Response(new User.Info(), 200, "success");
    }

    @PostMapping
    public DefaultResponse addUser(@RequestBody User.Info info){
        return new DefualtResponse();
    }
}
```

### 🖐🏻 여기서 `Inner Class`는 왜 `static` 으로 만들까?
- 외부 참조 유지된다는  것 == 메모리에 대한 참조가 유지 >> GC가 메모리 회수 x
    - 이는 __메모리 누수__ 를 부르는 치명적 단점
- 항상 외부 인스턴스의 참조를 통해야 함으로 성능 상 비효율적


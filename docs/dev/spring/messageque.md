## 메시지 큐 이해하기
> Message Queue(MQ)
- 프로세스(프로그램)간에 데이터 교환할 때 사용하는 통신 방법
    - 더 큰 개념으로 `MOM`(Message Oriented Middleware: 메시지 지향 미들웨어)
    - `MOM`: 비동기 메시지 사용하는 프로그램 간의 데이터 송수신 의미
    - MOM을 구현한 시스템 >> `MQ`(작업을 늦출 수 있는 유연성 제공)
- 메시지 교환할 때 >> `AMQP(Advenced Message Queuing Protocal)` 사용
    - `JMS`(Java Message Service): MOM을 자바에서 지원하는 표준 API
        - 다른 자바 어플리케이션 간 통신은 가능하지만 다른 MOM(AMQP, SMTP 등)끼리 통신 불가  ㅠ,ㅠ
    - `AMQP`: 프로토콜만 일치한다면 다른 AMQP를 사용한 어플리케이션과 통신 가능 :)
        - `wire-protocal` 제공 >> `octect stream` 이용해 다른 네트워크 사이에 데이터 전송할 수 있는 포맷

### 메시지 큐 장점 
- 비동기: 큐에 넣기 때문에 나중에 처리 가능
- 비동조: 애플리케이션과 분리 가능
- 탄력성: 일부가 실패 시 전체는 영향 없다 
- 과잉: 실패할 경우 재실행 가능
- 확장성: 다수의 프로세스들이 큐에 메시지를 보낼 수 있다

### 메시지 큐 사용처
- 다른 곳의 api로부터 데이터 송수신
- 다양한 어플리케이션에서 비동기 통신 가능 
- 이메일 발송 및 문서 업로드 가능
- 많은 양의 프로세스 처리

### 메시지 큐 종류 
- `Kafka`
- `RabbitMQ`
- `ActiveMQ`
- 공통적으로 모두 __✔ 비동기 통신__ 제공하고 보낸 사람과 받는 사람 __✔ 분리__ !!!
#### 다른 점은??
- `Kafka`
    - 처리량이 많은 분산 메시징 시스템 적합
- `RabbitMQ` `ActiveMQ`
    - 신뢰성 이쓴ㄴ 메시지 브로커가 필요한 경우 적합
    - 신뢰성이 카프카보다 높은거지 카프카가 신뢰성이 없다는 것은 아니다.

|메시지 큐 종류|장점|
|-------------|-----|
|`RabbitMQ`<br> AMQP 프로토콜을 구현해 높은 프로그램으로써 빠르고 쉽게 구성할 수 있으며 직관적|- 신뢰성,안정성<br>- 유연한 라우팅(메시지 큐가 도착하기 전에 라우팅 되며 플로그인 통해 더 복잡한 라우팅도 가능)<br>- 클러스터링(로컬네트워크에 있는 여러 RabbitMQ 서버를 논리적으로 클러스터링할 수 있고 논리적인 브로커도 가능)<br>- 관리 UI의 편리성(관리자 페이지 및 모니터링 페이지가 제공)<br>- 거의 모든 언어 및 운영체제 지원<br>- 오픈 소스로 상업적 지원 가능|
|`Kafka`<br> 확장성, 고성능, 높은 처리량 <br>특화된 시스템 >> 범용 메시징 시스템에서 제공하는 다양한 기능들 제공 x<br> __분산 시스템__ 을 기본으로 설계되어있기에 기존 메시징 시스템에 비해 분산 및 복제 구성 손쉽게 가능|- 대용량 실시간 로그 처리 특화<br>- AMQP 프로토콜이나 JMS API 사용하지 않고 단순한 메시지 헤더를 지닌 `TCP` 기반 프로토콜 사용함으로써 오버헤드 비교적 적음<br>- 노드 장애에 대한 대응성<br>- 프로듀서는 각 메시지를 배치로 브로커에게 전달하여 TCP/IP 라운드 트립 줄임<BR>- 메시지를 기본적으로 파일 시스템에 저장하여 별도의 설정을 하지 않아도 오류 발생 시 오류 지점부터 복구가 가능(기존 메시징 시스템은 메모리에 저장)<BR>- 메시지를 파일시스템에 저장하기 때문에 메시지가 많이 쌓여도 기존 메시징 시스템에 비해 성능이 크게 감소하지 않음<BR>- window 단위의 데이터를 넣고 꺼낼 수 있다|
|`ActiveMQ` <br>자바로 만든 오픈소스 메시지 브로커<br>`JMS 1.1` 을 통해 자바뿐 아니라 다른 언어를 사용하는 클라이언트 지원|- 다양한 언어와 프로토콜 지원<BR>- OpenWire를 통해 고성능의 java,C,C++,C# 클라이언트 지원<br>- Stomp를 통해 C, Ruby, Perl, Python, PHP 클라이언트가 다른 인기있는 메시지 브로커들과 마찬가지로 ActiveMQ 접근 가능<br>- Message Groups, Virtual Destinations, Wildcards와 Composite Destination 지원<br>- Spring 지원으로 ActiveMQ는 Spring Application에 쉽게 임베딩 가능, Spring의 xml 설정 메커니즘에 의해 쉽게 설정 가능<br>- Geronimo, JBoss 4, GlassFish, WebLogic과 같은 인기있는 J2EE 서버들과 함께 테스트됨<br>- 고성능의 저널을 사용할때에 JDBC 사용해 매우 빠른 영속성<BR>- `REST API` 통해 웹기반 메시징 API 지원<BR>- 웹 브라우저가 메시징 도구 될 수 있도록, Ajax 통해 순수한 DHTML을 사용한 웹 스트리밍 지원|

# ⭐ JPA 다대일 관계 작성하고 테스트해보자

## 개발 순서
### 1. 먼저 `application.yml`
> 설정이 복잡하면 __yml__ 형식이 편하다.

```yml
server:
  port: 9090

spring:
  datasource:
    url: jdbc:mysql://localhost:3306/jpa?serverTimezone=UTC&characterEncoding=UTF-8 
    username: root
    password: 1111
    driver-class-name: com.mysql.cj.jdbc.Driver

  jpa:
    database-platform: org.hibernate.dialect.MySQL5InnoDBDialect ✅ 방언 설정 실수 주의!
    hibernate:
      ddl-auto: create

    properties:
      hibernate:
        show_sql: true
        format_sql: true

logging:
  level:
    org.hibernate.SQL: debug

```

### 2. `Entity` 작성하자
> `Post` `Comment`

![image.png](../../img/manytoone.png)

- `Post.java`

```java
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "post")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private Long postId;
    private String title;
    private String author;
    private int likeCount;
    @Lob ✅ Large Object 데이터양 4기가 저장 가능!
    private String content;
}
```

- `Comment.java`

```java
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "comment")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="comment_id")
    private Long commentId;
    private String content;
    private String author;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

}
```


### 3. 이제 테스트를 해보자
#### 3-1. 먼저 `Repository` 를 만들어야 된다.

- `Post`

```java
import com.jpaentity.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {}
}
```

- `Comment`

```java
import com.jpaentity.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {
}
```


#### 3-2. 각 레포지토리의 테스트를 만들자

- `PostRepository`

```java
@ExtendWith(SpringExtension.class)
@SpringBootTest
class PostRepositoryTest {

    @Autowired
    private PostRepository postRepository;

    @Test
    public void save_post_확인() {
        postRepository.save(Post.builder()
        .title("title1")
        .author("hyunho")
        .likeCount(5)
        .content("content").build());
        List<Post> posts = postRepository.findAll();
        assertEquals(posts.get(0).getTitle(), "title1");
    }

}
```

- `CommentRepository`

```java
@ExtendWith(SpringExtension.class)
@SpringBootTest
class CommentRepositoryTest {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    @Test
    public void save_post_comment_확인() {
        Post post = postRepository.save(Post.builder()
        .title("title")
        .author("hyunho")
        .likeCount(5)
        .build());

        for(int i = 0; i < 10; i++){
            Comment comment = Comment.builder()
                    .author("frank" + i)
                    .content("content" + i).build();

            comment.setPost(post);
            commentRepository.save(comment);
        }

        List<Comment> comments = commentRepository.findAll();

        assertEquals(comments.get(0).getAuthor(), "frank0");
        assertEquals(comments.get(0).getPost().getAuthor(), "hyunho");
    }

}
```


### 4. 결과
![image-1.png](../../img/manytoone-result.png)


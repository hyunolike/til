## `ConcurrentHashMap ` 동기화 방식
- `map`
    - 수학적 용어 > 함수 자체 의미
    - ![image-8.png](./image-8.png)

### `HashMap` `HashTable` `ConcurrentHashMap` 
- `HashMap`: Thread에 안전하지 않음
- `HashTable`:Thread safe / 데이터 관련 함수에 synchronized 키워드 선언
- `ConcurrentHashMap`: Thread safe

### `Thread safe`
- 멀티 쓰레드 환경에서 개발자가 의도한대로 코드 동작하는 것 
- 어떤 함수나 변수, 객체가 여러 스레드에서 동시에 접근이 이뤄져도 수행 결과가 의도한 대로 나오는 것
- 따라서, `HashMap` 을 __Thread safe__ 하게 사용하기 위해 `ConcurrentHashMap` 사용!

### `ConcurrentHashMap` ? 
- 스레드가 안전한 연산을 할 수 있게 해주는 해시맵
- 읽기보다 쓰기가 많을 때 가장 적합

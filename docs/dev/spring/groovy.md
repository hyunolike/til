## Groovy 그루비
- JVM에서 실행되는 스크립트 언어(동적 타입 프로그래밍 언어)
- 문법 JAVA와 유사
- java 클래스 파일 그대로 groovy 클래스로 사용 가능

### 그루비로 테스트코드 작성 이유
- 가독성

### 1. Groovy data type 
- 별도의 타입 구체적으로 정의할 필요 없다. 
    - 구체적으로 지정해도 상관 없음.
- `def`

### 2. Groovy Strings
- 여러 줄로 이루어진 문자열 `""" """`

### 3. Groovy Closure
- java의 람다

### 4. Groovy Methods
- 메서드 정의는 자바와 동일
- 다른 점은 return을 명시하지 않아도 마지막 줄이 return 값이 된다는 점


## 🍃 시큐리티로 설정된 유저 정보 가져오는법 
> [참고 자료](https://itstory.tk/entry/Spring-Security-%ED%98%84%EC%9E%AC-%EB%A1%9C%EA%B7%B8%EC%9D%B8%ED%95%9C-%EC%82%AC%EC%9A%A9%EC%9E%90-%EC%A0%95%EB%B3%B4-%EA%B0%80%EC%A0%B8%EC%98%A4%EA%B8%B0)
> 현재 인증된 사용자의 정보 가져오는 방법은 무엇일까

### 1. `Bean`
- 전역에 선언된 `SecurityContextHolder`을 이용해 가져오는 방법
```java
Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal(); UserDetails userDetails = (UserDetails)principal; String username = principal.getUsername(); String password = principal.getPassword();
```

### 2. `Controller` 에서 사용자 정보 받기
- `Principal` 객체에 직접 접근 (`@Controller`로 선언된 bean객체에서 사용 가능)
```java
⭐ Princlpal
@Controller 
public class SecurityController { 
    @GetMapping("/username") 
    @ResponseBody public String currentUserName(Principal principal) { 
        return principal.getName(); } 
        }

⭐ Authentication
@Controller public class SecurityController { 
    @GetMapping("/username")
    @ResponseBody public String currentUserName(Authentication authentication) {
    UserDetails userDetails = (UserDetails) authentication.getPrincipal(); 
    return userDetails.getUsername(); } }
```

### 3. `@AuthenticationPrincipal` 
- `Spring Security 3.2` 부터 annotation 이용해 현재 로그인한 사용자 객체 인자 주입 가능

```java
@Controller 
public class SecurityController { 
    @GetMapping("/messages/inbox") 
    public ModelAndView currentUserName(@AuthenticationPrincipal CustomUser customUser) { String username = customUser.getUsername(); // .. find messages for this user and return them ... } }
```

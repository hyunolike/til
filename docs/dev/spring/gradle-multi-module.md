## Gradle 이용해 멀티 모듈 
> 모듈마다 각각 서버를 띄울 수 있다. :)

![image.png](../../img/gradle-result.png)
![image.png](../../img/server-two.png)

### 1. `Spring boot` 기본 코드 만들기
- `start.spring.io` 에서 생성

### 2. 3개의 모듈 생성
- `module-commom`
- `module-user`
- `module-board`

### 3. 프로젝트 `src` 파일 이동
- 각각의 모듈에 `src` 파일 복사해서 붙여넣기
- 기존 프로젝트의 `src` 파일은 삭제

### 4. `setting.gradle` 수정

```groovy
rootProject.name = 'gradel-multi-module'
include 'module-board'
include 'module-common'
include 'module-use
```

### 5. `build.gradle` 수정

```groovy
buildscript {
	ext {
		springBootVersion = '2.1.4.RELEASE'
	}
	repositories {
		mavenCentral()
	}

	dependencies {
		classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
		classpath "io.spring.gradle:dependency-management-plugin:0.6.0.RELEASE"
	}
}

subprojects {
	apply plugin: 'java'
	apply plugin: 'org.springframework.boot'
	apply plugin: 'io.spring.dependency-management'

	sourceCompatibility = 1.8

	repositories {
		mavenCentral()
	}

	configurations {
		compileOnly {
			extendsFrom annotationProcessor
		}
	}

	// 모든 모듈에서 사용하는 라이브러리
	dependencies {
		implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
		implementation 'org.springframework.boot:spring-boot-starter-freemarker'
		implementation 'org.springframework.boot:spring-boot-starter-web'
		implementation 'org.springframework.boot:spring-boot-starter-actuator'
		implementation 'org.springframework.boot:spring-boot-starter-data-redis'
		//embedded-redis
		implementation 'it.ozimov:embedded-redis:0.7.2'
		implementation 'io.springfox:springfox-swagger2:2.6.1'
		implementation 'io.springfox:springfox-swagger-ui:2.6.1'
		implementation 'net.rakugakibox.util:yaml-resource-bundle:1.1'
		implementation 'com.google.code.gson:gson'
		compileOnly 'org.projectlombok:lombok'
		runtimeOnly 'com.h2database:h2'
		runtimeOnly 'mysql:mysql-connector-java'
		annotationProcessor 'org.projectlombok:lombok'
		testImplementation 'org.springframework.security:spring-security-test'
		testImplementation 'org.springframework.boot:spring-boot-starter-test'
	}
}

project(':module-common') {
	// common 모듈에만 필요한 라이브러리가 발생하면 이곳에 추가한다.
	dependencies {
		implementation 'org.springframework.boot:spring-boot-starter-security'
		implementation 'io.jsonwebtoken:jjwt:0.9.1'
	}
}

project(':module-user') {
	// user 모듈에만 필요한 라이브러리가 발생하면 이곳에 추가한다.
	dependencies {
		implementation project(':module-common')
	}
}

project(':module-board') {
	// board 모듈에만 필요한 라이브러리가 발생하면 이곳에 추가한다.
	dependencies {
		implementation project(':module-common')
	}
}
```

### 6. `module-common build.gradle` 수정
- `common 모듈` 은 실행가능한 bootJar로 패키징할 필요가 없기 때문에 아래 내용 추가

```groovy
plugins {
    id 'java'
}

bootJar {
    enabled = false
}
jar {
    enabled = true
}

group 'com.muti-module'
version '0.0.1-SNAPSHOT'

repositories {
    mavenCentral()
}

dependencies {
    testCompile group: 'junit', name: 'junit', version: '4.12'
}
```

### 7. 각각의 모듈 설정
- `build.gradle` 파일의 내용을 삭제
- `SpringBoot Application` 각각 작성
- 마지막은 실행 :)

### 끝!
![image.png](../../img/folder-result.png)

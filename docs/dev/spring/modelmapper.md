## `ModelMapper` 
> [공식 문서](http://modelmapper.org/)
- 어떤 Object에 있는 필드값들을 자동으로 원하는 Object으로 Mapping 해줌
- `getter` `setter` 의 작성할 필요 없어진다!! 👌

### 언제 사용?
> [참고 자료](https://velog.io/@kimview/ModelMapper)
![image-7.png](./image-7.png)




# 👨‍🚀Jenkins 실습
## 1. 🐳젠킨스 설치
![image-11.png](./image-11.png)
- [도커 실행 관련 공식 문서](https://github.com/jenkinsci/docker/blob/master/README.md)
- `docker run -p 8080:8080 -p 50000:50000 --restart always jenkins/jenkins:lts-jdk11`
    - `docker run`: 이미지 실행 명령어
    - `-d`: 백그라운드로 실행시키는 옵션
    - `-p 8080:8080  -p 50000:50000`: 포트포워딩 / 포트 8080, 50000 2개 사용하기 때문 (젠킨스)
- 실행 화면
    - ![image-12.png](./image-12.png)
- 비밀번호 입력
    - ![image-13.png](./image-13.png)
- ⭐ 생성 완료
    - ![image-14.png](./image-14.png)
- ✔ 로컬에선 인터넷 연결이 필요함으로 도커 오프라인!! >> 사내인프라인 경우 오프라인 ㅠ,ㅠ
    - 오프라인 상황에서 해당 >> [젠킨스 플러그인 다운로드 사이트](https://plugins.jenkins.io/) 에서 다운로드 받자
    - ![image-16.png](./image-16.png)
    - ![image-15.png](./image-15.png)
### 1.1 플러그인 설치 (수동) ✔
- ![image-17.png](./image-17.png)
- 위에 나와있는 플러그인 모두 설치되어있는 상태여야 정상적으로 설치 가능해짐!!
## 2. Gitlab 프로젝트 생성
> [참고 자료](https://velog.io/@hmyanghm/Jenkins%EC%97%90-GitLab-%EC%A0%80%EC%9E%A5%EC%86%8C-%EC%97%B0%EB%8F%99)

## 🍃javadoc
- 코드 인수인계, 기능 명세화, 자바 소스 파일들을 문서화할 때
- html 형식 설명, 하이퍼링크 생성 >> `javadoc`

|기능|설명|
|------|------|
|`@author`|코드 소스 작성자|
|`@deprecated`|해당 클래스(구현체)의 삭제 또는 지원이 중단되는 것을 알려줌|
|`@exception`|예외처리할 수 있는 것들을 정의, 알파벳 순|
|`@param`|매개변수 메서드, 생성자 설명|
|`@return`|리턴값 설명|
|`@see`|파일이 참조하는 다른 클래스와 메서드 등|
|`@serial`|Serializeable 인터페이스에 사용|
|`@serialData`|writeObject writeExternal 메소드로 작성된 데이터 설명|
|`@serialField`|serialPersistnetFields 모든 배열에 사용됨|
|`@since`|해당 클래스가 추가된 버전|
|`@throws`|@exception 처럼 예외처리하는 것들을 정의|
|`@version`|구현체, 패키지 버전 명시|

### 예시 코드
```java
/**
 * 메소드 연결 -> {@link #메소드()} 
 *    
**/
```

```yml
FROM openjdk:11
ARG JAR_FILE=docker-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} myboot.jar
ENTRYPOINT ["java", "-jar", "/myboot.jar"]
```

![image-1.png](./image-1.png)
![image.png](./image.png)

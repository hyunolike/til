## 🐳 도커
- 운영체제 >> 여러 소프트웨어
    - 설치하는게 귀찮아...
    - 다 세팅되어있는건 없을까?? 네트워크를 통해서 사용할순 없을까??
- 📢 `host`: 운영체제가 설치된 컴퓨터 >> 주인
- 📢 `container`: host에 설치된 각각의 격리된 환경

![image.png](./img/image.png)

#### ⭐ 리눅스 운영체제 >> __컨테이너 기술__ 이용 >> `Docker` 
> 리눅스 운영체제만의 앱 실행방법 >> 컨테이너 기술 

- 리눅스 운영체제가 아니면?? 어떻게 사용해??
    - 가상머신 > 리눅스 운영체제 ✔ 

![image.png](./img/image1.png)

- docker `cli` 로 해야 효과적인 퍼포먼스 사용 가능 ✔
- 1개의 image는 여러개의 container 만들 수 있다!! 기존 앱이랑 똑같다.

|`📚app store`|`🐳docker hub`|
|:---------:|:-----------:|
|program|image|
|process|container|

### 네트워크에 대해 알아보자🖐🏻
✔ `기존 네트워크`
![image.png](./img/image3.png)

✔ `Docker 네트워크`
![image.png](./img/image4.png)
![image.png](./img/image5.png)
![image.png](./img/image6.png)

### 나의 웹앱을 만들어보자✌🏻
> 직접 조작해보자 :)
- 도커 데스크탑 > container cli 선택
- cli > `docker exec -it 컨테이너명 /bin/sh` 요즘에는 `/bin/bash` ✔ 
    - `it` 시스템 유지할수 있게!!
- 나가기 > `exit`

#### `index.html` 파일 수정하기
1. `htdocs` 파일 이동
2. `apt update` > `apt install nano`
3. `nano index.html`
4. 해당 index 파일 수정

![image.png](./img/image7.png)

### 위와 같이 직접 수정하면 위험해...🤣
- 컨테이너 사용 이유 > 필요할때 사용하고 안사용하기 위해!
- __실행환경__ : 컨테이너
- __파일수정작업__ : 호스트

![image.png](./img/image8.png)

- `docker run -p 8080:80 -v ~/Desktop/htdocs:/usr/local/apache2/htdocs/ httpd`
    - `~/Desktop/htdocs` 호스트에서 수정된 파일 위치
    - `-v` 볼륨
- 호스트에서 파일이 수정되고 노출되기때문에 호스트환경에서 버전관리 백업정책 에디터 코드 편집이라는 장점 :)



## 🐳spring boot + mysql + docker compose
- 해당 파일 위치에서 `docker-compose up` 실행해준다.
- 아래 3가지 파일 모두 루트 파일에 위치 시킨다.

### `.env`
```
MYSQDB_USER=root
MYSQLDB_ROOT_PASSWORD=1234
MYSQLDB_DATABASE=docker_test
MYSQLDB_LOCAL_PORT=3309
MYSQLDB_DOCKER_PORT=3306

SPRING_LOCAL_PORT=6868
SPRING_DOCKER_PORT=8080
```

### `Dockerfile`
```Dockerfile
FROM openjdk:11

ARG JAR_FILE=./target/docker-compose-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} deploy.jar
ENTRYPOINT ["java", "-jar", "/deploy.jar"]
```

### `docker-compose.yml`
```yml
version: "3.8"

services:
  mysqldb:
    image: mysql:5.7
    restart: unless-stopped
    env_file: ./.env
    environment:
      - MYSQL_ROOT_PASSWORD=$MYSQLDB_ROOT_PASSWORD
      - MYSQL_DATABASE=$MYSQLDB_DATABASE
    ports:
      - $MYSQLDB_LOCAL_PORT:$MYSQLDB_DOCKER_PORT
  app:
    depends_on:
      - mysqldb
    build: /
    restart: on-failure
    env_file: ./.env
    ports:
      - $SPRING_LOCAL_PORT:$SPRING_DOCKER_PORT
    environment:
      SPRING_APPLICATION_JSON: '{
        "spring.datasource.url"  : "jdbc:mysql://mysqldb:$MYSQLDB_DOCKER_PORT/$MYSQLDB_DATABASE?useSSL=false",
        "spring.datasource.username" : "$MYSQLDB_USER",
        "spring.datasource.password" : "$MYSQLDB_ROOT_PASSWORD",
        "spring.jpa.properties.hibernate.dialect" : "org.hibernate.dialect.MySQL5InnoDBDialect",
        "spring.jpa.hibernate.ddl-auto" : "update"
      }'

    stdin_open: true
    tty: true
```

## 구조
![image-2.png](./image-2.png)

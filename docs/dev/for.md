## 향상된 for문
- `for-each` 문 형식
- 오직 __배열 값__ 을 가져다 사용 
- 수정할 수 없어
- 기존 for문과의 성능 차이 ❌

```java
for(변수타입 변수이름 : 배열이름){
    실행부분;
}

List arr = new ArrayList();
arr.add(1);
arr.add(2);

for(int i : arr){
    System.out.print(i); // 1 , 2
}
```



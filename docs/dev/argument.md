## argument vs parameter

|단어|번역|의미|
|---|------|----|
|`Parameter`|매개변수|함수와 메서드 입력 변수(Variable) 명|
|`Argument`|전달인자|함수와메서드의 입력 값(Value)|

### Parameter

```java
def concat(str1, str2){
    return a + b;
}
```

### Argument

```java
concat("str1", "str2")
```

# Typescript 
- [typescript 개념](https://github.com/hyunolike/info-docs/blob/main/typescript/study.md)
- [react-typescript 사용할래](https://github.com/hyunolike/info-docs/blob/main/typescript/react.md)
- [`Type` vs `Interface`](https://github.com/hyunolike/info-docs/blob/main/typescript/type-interface.md)
- [typescript 기초 강의](https://github.com/hyunolike/info-docs/blob/main/typescript/lecture.md)
- [⭐️ typescript](https://github.com/hyunolike/info-docs/blob/main/typescript/typescript.md)

# ⭐ JSON API 개발
## [News API](https://newsapi.org/) 를 활용해 똑같이 JSON API 개발해보자
> Spring boot

## 개발 순서
### 1. News API 아키텍처 파악하기

```json
{
    "status": "ok",
    "totalResults": 2057,
    "articles": [
        {
        "source": {
        "id": "engadget",
        "name": "Engadget"
        },
        "author": "Amrita Khalid",
        "title": "Paramount+ hits 32.8 million subscribers; will offer Showtime for a fee",
        "description": "A lot is changing at ViacomCBS — which changed its name today to Paramount Global\r\n. In an investor presentation\r\n on Tuesday, the company announced that its streaming service, Paramount+\r\n (formerly known as CBS All Access), hit 32.8 million subscribers duri…",
        "url": "https://www.engadget.com/paramount-plus-showtime-fourth-quarter-subscribers-010947475.html",
        "urlToImage": "https://s.yimg.com/os/creatr-uploaded-images/2022-02/6a168e00-8ec4-11ec-bffb-5830d9cd5237",
        "publishedAt": "2022-02-16T01:09:47Z",
        "content": "A lot is changing at ViacomCBS which changed its name today to Paramount Global\r\n. In an investor presentation\r\n on Tuesday, the company announced that its streaming service, Paramount+\r\n (formerly k… [+2167 chars]"
        },
        {
        "source": {
        "id": "engadget",
        "name": "Engadget"
        },
        "author": "Kris Holt",
        "title": "There are now more than 500 million Epic Games accounts",
        "description": "Epic Games\r\n says there are now more than 500 million Epic Games accounts\r\n. Many of those are used to buy and play PC games, and accounts are also utilized on consoles and mobile devices for the likes of Fortnite\r\n and Rocket League\r\n.Those who play Fortnite…",
        "url": "https://www.engadget.com/epic-games-accounts-500-million-165908445.html",
        "urlToImage": "https://s.yimg.com/os/creatr-uploaded-images/2022-02/fdfbd600-8f48-11ec-9bd3-67ebfdc858c7",
        "publishedAt": "2022-02-16T16:59:08Z",
        "content": "Epic Games\r\n says there are now more than 500 million Epic Games accounts\r\n. Many of those are used to buy and play PC games, and accounts are also utilized on consoles and mobile devices for the lik… [+2163 chars]"
        }
    ]
}
```
- 구성은 크게 `String` `array` `object` 로 구성되어 있다.

![image.png](../../img/json.png)

### 2. 필요한 DTO 생각하기
- 보통 요청, 응답시 `@RequestParam` 보다는 `DTO` 생성해서 받아오는 걸 선호한다.
- 스프링 부트에서는 `@RestControllor` 가 `Object > JSON` 으로 자동으로 바꿔준다.

![image.png](../../img/json-dto.png)

### 3. DTO 개발
- `NewsDTO.java`

```java
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class NewsDto {
    private String status;
    private int totalResults;
    private List<ArticlesDto> articles; ⭐ 배열

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public List<ArticlesDto> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticlesDto> articlesDtos) {
        this.articles = articlesDtos;
    }
}
```

- `ArticlesDTO`

```java
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ArticlesDto {
    private SourceDto source; ⭐ 객체
    private String author;
    private String title;
    private String description;
    private String url;
    private String ulrToImage;
    private String publishedAt;
    private String content;

    public SourceDto getSource() {
        return source;
    }

    public void setSource(SourceDto source) {
        this.source = source;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUlrToImage() {
        return ulrToImage;
    }

    public void setUlrToImage(String ulrToImage) {
        this.ulrToImage = ulrToImage;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
```

- `SourceDTO`

```java
public class SourceDto {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
```

### 4. 최종 컨트롤러 연결

```java
@RestController
@RequestMapping("/news")
public class NewsController {
    @GetMapping("/get")
    public NewsDto get(@RequestBody NewsDto newsDto) {
        return newsDto;
    }
}
```

![image.png](../../img/json-result.png)



## ☕ 내부 클래스와 익명 클래스

### 내부 클래스(Inner Class)
- 내부 클래스는 클래스 내에 선언된다는 점 제외하고 일반적인 클래스와 동일
- 보통 2개의 클래스가 서로 긴밀한 관계가 있을 때 사용
#### 내부 클래스 장점 
1. 내부 클래스에서 외부 클래스의 멤버들을 쉽게 접근 가능
2. 코드의 복잡성 줄임(캡슐화)

```java
class A{
    class B{

    }
}
```
- 내부 클래스 B는 A말고 다른 클래스에서 잘 사용되지 않는 것이어야 됨

#### 내부 클래스 종류와 특징
|내부클래스|특징|
|:----:|------|
|인스턴스 클래스|외부 클래스의 멤버변수 선언 위치에 선언하며, 외부 클래스의 인스턴스 멤버처럼 다뤄짐. <br>주로 외부 클래스의 인스턴스멤버들과 관련된 작업에 사용될 목적으로 선언|
|스태틱 클래스|외부 클래스의 멤버변수 선언 위치에 선언하며, 외부 클래스의 static 멤버처럼 다뤄진다. <br>주로 외부 클래스의 static멤버, 특히 static메서드에서 사용될 목적으로 선언|
|지역 클래스|외부 클래스의 메서드나 초기화블럭 안에 선언하며, 선언된 영역 내부에서만 사용 가능|
|⭐익명 클래스|클래스의 선언과 객체의 생성을 동시에 하는 이름없는 클래스(일회용)|

```java
class Outer{
    class InstanceInner{ } ✔ 인스턴스 클래스
    static class StaticInner{ } ✔ 스태틱 클래스

    void method1(){
        class LocalInner{ } ✔ 지역 클래스
    }
}
```

### 익명 클래스
- 클래스의 선언과 객체의 생성 동시
- 단 한번만 사용될 수 있고 오직 하나의 객체만을 생성하는 __일회용 클래스__
- 이름이 없기에 생성자도 가질수 없다.

```java
new 부모클래스 이름(){

}

new 구현인터페이스이름(){

}
```

```java
class test{
    Object iv = new Object(){
        void method();
    }

    static Object cv = new Object(){
        void method();
    }

    void method(){
        Object iv = new Object(){
            void method();
        }
    }
}
```

`템플릿 콜백 패턴 중 ...`
```java
public class Client{
    public static void main(String[] args){
        Solider rambo = new Solider();

        rambo.runContext(new Strategy(){ ✅ 익명 클래스
            @Override
            public void runStrategy(){
                System.out.println("총 탕탕탕");
            }
        })
    }
}
```



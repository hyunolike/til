## Jsoup 
> [참고자료](https://offbyone.tistory.com/116) <br>
> [참고자료](https://micropilot.tistory.com/2427)
- HTML 파싱 Java 라이브러리
- DOM, CSS 및 JQuery 와 같은 방법을 사용해 데이터 추출하고 조작하는 API 제공

### 실습
#### 1. 먼저 그레들 설정 후, 서비스단 개발
```java
package com.example.crawling.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class JsoupService {
    public String jsoupGet() throws IOException {
        Document doc = Jsoup.connect("https://www.google.com/").get();
        String title = doc.title();
        System.out.println(title);
        return title;
    }

    public String jsuopPost() throws IOException {
        Document doc = Jsoup.connect("https://www.techm.kr/news/articleView.html")
                .data("idxno", "101525")
                .timeout(2000)
                .post();

//        Elements imgs = doc.getElementsByTag("img");
//        if(imgs.size() > 0){
//            return imgs.attr("src");
//        }else{
//            return "정보가 존재하지 안항 ㅠ,ㅠ";
//        }

        Elements img = doc.select("figure[data-type=photo] img[src]");
        String imgSrc = img.attr("src");

        return imgSrc;
    }
}

```

#### 2. 컨트롤러단
```java
package com.example.crawling.controller;

import com.example.crawling.service.JsoupService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
public class JsoupController {

    private final JsoupService jsoupService;

    @GetMapping("/jsoup/get")
    public String jsoupGet() throws IOException {
        return jsoupService.jsoupGet();
    }

    @GetMapping("/jsoup/post")
    public String jsoupPost() throws IOException {
        return jsoupService.jsuopPost();
    }
}
```
#### 3.결과
- ![image.png](./image.png)
- ![image-1.png](./image-1.png)
- ![image-2.png](./image-2.png)

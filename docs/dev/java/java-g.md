
```java
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Java20230330 {
    public static void main(String[] args) {

        List<ResultObject> resultObjects = new ArrayList<>();

        for(int i=0; i<10; i++){
            ResultObject resultObject = new ResultObject();
            resultObject.setCode1("code1");
            resultObject.setCode2("code2");

            resultObjects.add(resultObject);
        }

        Response<List<ResultObject>> result = Response.of(resultObjects);

        System.out.println(result.toString());
    }
}

class Response<D> {
    private String code;
    private D result;

    public void setCode(String code) {
        this.code = code;
    }

    public void setResult(D result) {
        this.result = result;
    }

    public static <D> Response<D> of (D result){
        Response<D> response = new Response<>();
        response.setCode("test code");
        response.setResult(result);

        return response;
    }

    @Override
    public String toString() {
        return "Response{" +
                "code='" + code + '\'' +
                ", result=" + result +
                '}';
    }
}

class ResultObject {
    private String code1;
    private String code2;

    public void setCode1(String code1) {
        this.code1 = code1;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }
}

```
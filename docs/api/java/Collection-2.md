## Collection 2차원 배열
```java
 List<List<Integer>> result = new ArrayList<>();
        ArrayList<Integer> el1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> el2 = new ArrayList<>(Arrays.asList(4, 5, 6));

        result.add(el1);
        result.add(el2);

        System.out.println(result);

        for(int i=0; i<result.size(); i++){
            Collections.sort(result.get(i), Collections.reverseOrder());
        }

        System.out.println(result);
```

### 콜렉션 2차원 배열 쉽게 배열요소로 변경하는 방법
```java
result.toArray();
```

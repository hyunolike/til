## PriorityQueue
```java
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        priorityQueue.offer(10);
        priorityQueue.offer(12);
        priorityQueue.offer(1);
        priorityQueue.offer(2);

        System.out.println(priorityQueue);

        PriorityQueue<Integer> highNum = new PriorityQueue<>(Collections.reverseOrder());
        highNum.offer(10);
        highNum.offer(1);
        highNum.offer(2);
        highNum.offer(20);

        System.out.println(highNum);
```
![image.png](./image.png)

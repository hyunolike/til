## Stream 최종 연산 결과값 받기
```java
public class Test {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(Arrays.asList(1, 1, 1, 2, 2, 3, 3, 4, 5 , 67, 8));
        int[] arr = list.stream().mapToInt(Integer::intValue).toArray();
        
        System.out.println(
                Arrays.stream(arr).distinct().filter(i -> i % 2 == 0).max().getAsInt() ⭐
        );
    }
}
```

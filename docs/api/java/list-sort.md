## List.sort()
> [참고자료](https://codechacha.com/ko/java-sort-list/)
- 정렬 3가지
    - `Collections.sort()`
    - `List.sort()`

### 1. Collections.sort()
- `Collections.sort(list)`
- `Collections.sort(list, Collections.reverseOrder())`
- `Collections.sort(list, String :: compareToIgnoreCase)`

### 2. List.sort()
- `sort(Comparator.naturalOrder())`: 오름차순 정렬
- `sort(Comparator.reverseOrder())`

## Deque
> [참고자료](https://hbase.tistory.com/128)

### 1. Deque 생성
```java
Deque<> d = new ArrayDeque<>();
Deque<> d = new LinkedBlockingDeque<>();
Deque<> d = new ConcurrentLinkedDeque<>();
Deque<> d = new LinkedList<>();
```

## Arrays.toString()
```java
public class PrintArray {    
    public static void main(String[] args) {        
        int[] arr = { 1, 2, 3, 4, 5 };         
        System.out.println(arr);  // ??    ⭐ 해당 메모리의 주소값
        System.out.println(Arrays.toString(arr)); 📌 우리가 원하는 배열의 모습이 나오게 됨!!
    }
}

```

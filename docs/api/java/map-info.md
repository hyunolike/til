## Map `map.entrySet` `Map.Entry()`
> [참고자료](https://tychejin.tistory.com/31)

### 📌Map 전체 값 출력 방법
#### 1. entrySet()
#### 2. keySet()

### 코드
```java
map.entrySet().stream().map(Map.Entry<Integer, String>::getKey).collect(Collectors.toList())

// 방법 06 : Stream 사용
map.entrySet().stream().forEach(entry-> {
	System.out.println("[key]:" + entry.getKey() + ", [value]:"+entry.getValue());
});
	        
// Stream 사용 - 내림차순
map.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(entry-> {
	System.out.println("[key]:" + entry.getKey() + ", [value]:"+entry.getValue());
});

// Stream 사용 - 오름차순
map.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.reverseOrder())).forEach(entry-> {
	System.out.println("[key]:" + entry.getKey() + ", [value]:"+entry.getValue());
});
```

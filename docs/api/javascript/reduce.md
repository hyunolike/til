```js
function sum(...num) { 💡나머지 매개변수 받아오는거
  let total = 0;
  console.log(
    num.reduce((acc, el) => {
      return (acc += el);
    }, 0)
  );
}

sum(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

```
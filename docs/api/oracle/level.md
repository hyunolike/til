## level
> [참고자료](https://become.tistory.com/entry/Oracle-%EA%B8%B0%EC%B4%88-level%EC%9D%84-%EC%9D%B4%EC%9A%A9%ED%95%9C-%EB%82%A0%EC%A7%9C-%EB%B0%8F-%EC%8B%9C%EA%B0%84-dummy-%ED%85%8C%EC%9D%B4%EB%B8%94-%EB%A7%8C%EB%93%A4%EA%B8%B0)
- 🐯 오라클 사용하면 세상의 모든것이 표현 가능하대!!


```sql
select level from dual
connect by level <= 10;

select '2022년' || to_char(level, 'FM09') || '월' as no
from dual
connect by level <=12;
```

### 결과
![image.png](./image.png)

## Java API 🙄
### 1. class Object
> [참고자료](https://cr.openjdk.java.net/~iris/se/11/latestSpec/api/java.base/java/util/Objects.html)

|method|description|
|--------|--------|
|requireNonNull(T obj)|지정된 개체 참조가 null 아닌지 확인|

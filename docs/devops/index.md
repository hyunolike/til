## DevOps
- [`우아한 형제들` - CI / CD](https://github.com/hyunolike/info-docs/blob/main/devops/jenkins-ci-cd.md)
- [무중단 배포 아키텍처](https://github.com/hyunolike/info-docs/blob/main/devops/zero-downtime-deployment.md)
- [젠킨스 스크립트](https://github.com/hyunolike/info-docs/blob/main/devops/jenkins-script.md)
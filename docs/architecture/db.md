## master(source) + slave(replica)
> [참고 자료](https://medium.com/bina-nusantara-it-division/the-master-slave-database-concept-for-beginners-8a3884896b14)
- ![image-4.png](./image-4.png)
- 위 그림은 마스터가 1개 슬레이브 1개 구성됨
    - 하지만 보통 마스터 1개 슬레이브 여러개로 구성된다! ✔
- 📌`master db`: 실제 데이터 보관(쓰기)
- 📌`slave db`: 읽기만 수행 >> 사이트 안전성 위한 목적

## 요약
- 사용 목적 >> 시스템 안정화
- 데이터 동기화(마스터 <--> 슬레이브) >> 데이터 복제

## master-slave
![image-5.png](./image-5.png)

## redis(cache) + master-slave
![image-6.png](./image-6.png)
## ❌ 이거는 아니래 ㅠ,ㅠ 
![image-7.png](./image-7.png)

# Android
- [Java SSL 인증서 `Certificate`](https://github.com/hyunolike/info-docs/blob/main/android/java-ssl.md)
- [Architecture Pattern](https://github.com/hyunolike/info-docs/blob/main/android/architecture-pattern.md)
- [안드로이드 4대 컴포넌트(구성요소)](https://github.com/hyunolike/info-docs/blob/main/android/4-component.md)
- [FCM 서버 `Firebase`](https://github.com/hyunolike/info-docs/blob/main/android/fcm.md)
- [안드로이드 키스토어 - `Shared Preference` / 민감정보 저장](https://github.com/hyunolike/info-docs/blob/main/android/keystore.md)
- [파이어베이스](#)
- [안드로이드 액티비티 생명주기](#)
- [안드로이드 프래그먼트 생명주기](#)

## html5
- [`input` `button` 공통점&차이점](https://github.com/hyunolike/info-docs/blob/main/html/input-button.md)
- [글자 밑줄 긋기](https://github.com/hyunolike/info-docs/blob/main/html/text-decoration.md)
- [⭐`viewport` 사용](https://github.com/hyunolike/info-docs/blob/main/html/viewport.md)
- [⭐메타태그 종류](https://github.com/hyunolike/info-docs/blob/main/html/metatag.md)
- [`html` type, value, name](https://github.com/hyunolike/info-docs/blob/main/html/type-value-name.md)
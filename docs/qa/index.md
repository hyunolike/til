## QA
- [체크리스트 CL](https://github.com/hyunolike/info-docs/blob/main/qa/cl.md)
- [QA 스타트업 구축기](https://github.com/hyunolike/info-docs/blob/main/qa/qa-process.md)
- [기획 리뷰](https://github.com/hyunolike/info-docs/blob/main/qa/review.md)
- [토스 - QA](https://github.com/hyunolike/info-docs/blob/main/qa/toss-qa.md)
- [QA 테스트 결과 분류](https://github.com/hyunolike/info-docs/blob/main/qa/qa-test-word.md)